# Configurações necessárias:

Banco de dados: MySQL
PHP version > 7
CERTIFICADO SSL (https)



Arquivo de banco: banco.sql

https://gitlab.com/valerio.fabio/vision/-/blob/master/banco.sql



## Configuração do banco:

```
  private $host  = 'localhost';
  private $dbName = 'NOME-DO-BANCO';
  private $user  = 'USUARIO';
  private $pass  = 'SENHA';
  private $port  = '3307';
```

https://gitlab.com/valerio.fabio/vision/-/blob/master/painel/php/db.class.php



## Acesso ao painel

**http://seu-dominio.com**/painel

Usuário: patricia@agenciamgweb.com

Senha: 1q2w3e4r5t

