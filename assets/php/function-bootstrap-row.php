<?php
function coluna_row($col){

    switch ($col) {
        case 'col-1': $result = 'col-12'; break;
        case 'col-2': $result = 'col-md-6 col-sm-12'; break;
        case 'col-3': $result = 'col-xl-4 col-md-6 col-sm-12'; break;
        case 'col-4': $result = 'col-xl-3 col-md-6 col-sm-12'; break;
        case 'col-6': $result = 'col-xl-2 col-md-6 col-sm-12'; break;
        case 'col-12': $result = 'col-xl-1 col-md-6 col-sm-12'; break;
    }

        return $result;
}


function coluna_row_number($col){

    switch ($col) {
        case 'col-1': $result = '1'; break;
        case 'col-2': $result = '2'; break;
        case 'col-3': $result = '3'; break;
        case 'col-4': $result = '4'; break;
        case 'col-6': $result = '6'; break;
        case 'col-12': $result = '12'; break;
    }

        return $result;
}
?>