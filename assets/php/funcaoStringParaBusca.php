<?php

function stringParaBusca($str)
{
	//Transformando tudo em min�sculas
	$str = trim(strtolower($str));

	//Tirando espa�os extras da string... "tarcila  almeida" ou "tarcila   almeida" viram "tarcila almeida"
	while ( strpos($str,"  ") )
		$str = str_replace("  "," ",$str);
	
	//Agora, vamos trocar os caracteres perigosos "�,�..." por coisas limpas "a"
	$caracteresPerigosos = array ("Ã","ã","Õ","õ","á","Á","é","É","í","Í","ó","Ó","ú","Ú","ç","Ç","à","À","è","È","ì","Ì","ò","Ò","ù","Ù","ä","Ä","ë","Ë","ï","Ï","ö","Ö","ü","Ü","Â","Ê","Î","Ô","Û","â","ê","î","ô","û","!","?",",","“","”","-","\"","\\","/");
	$caracteresLimpos    = array ("a","a","o","o","a","a","e","e","i","i","o","o","u","u","c","c","a","a","e","e","i","i","o","o","u","u","a","a","e","e","i","i","o","o","u","u","A","E","I","O","U","a","e","i","o","u",".",".",".",".",".",".","." ,"." ,".");
	$str = str_replace($caracteresPerigosos,$caracteresLimpos,$str);
	
	//Agora que n�o temos mais nenhum acento em nossa string, e estamos com ela toda em "lower",
	//vamos montar a express�o regular para o MySQL
	$caractresSimples = array("a","e","i","o","u","c");
	$caractresEnvelopados = array("[a]","[e]","[i]","[o]","[u]","[c]");
	$str = str_replace($caractresSimples,$caractresEnvelopados,$str);
	$caracteresParaRegExp = array(
		"(a|ã|á|à|ä|â|ã|á|à|ä|â|Ã|Á|À|Ä|Â|Ã|Á|À|Ä|Â)",
		"(e|é|è|ë|ê|é|è|ë|ê|É|È|Ë|Ê|É|È|Ë|Ê)",
		"(i|í|ì|ï|î|í|ì|ï|î|Í|Ì|Ï|Î|Í|Ì|Ï|Î)",
		"(o|õ|ó|ò|ö|ô|õ|ó|ò|ö|ô|Õ|Ó|Ò|Ö|Ô|Õ|Ó|Ò|Ö|Ô)",
		"(u|ú|ù|ü|û|ú|ù|ü|û|Ú|Ù|Ü|Û|Ú|Ù|Ü|Û)",
		"(c|ç|Ç|ç|Ç)" );
	$str = str_replace($caractresEnvelopados,$caracteresParaRegExp,$str);
	
	//Trocando espa�os por .*
	$str = str_replace(" ",".*",$str);
	
	//Retornando a String finalizada!
	return $str;
}


/* exemplo */


/* 
   if( !empty( $_POST['busca'] ) ):
       include('busca_complexa/funcaoStringParaBusca.php');
       $str = stringParaBusca($_POST['busca']);
       $where = " pro_produto REGEXP \"" . $str . "\"";
   else:
       $where = "pro_id > 0";
   endif;

	$vis = new CONNECT();
	$vis->SQLS(1, 'produtos', $where, "pro_produto", "ASC", $_GET['pagina'], "?page=produtos/visualizar");
 */
 
?>