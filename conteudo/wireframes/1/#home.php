	
<section class="slider p-0" id="topo">
	<div class="container">
		<div class="row d-flex align-items-center">

			<div class="col-xl-8 text-center mt-5 mt-xl-0"> 
						<p class="wow fadeInDown">
							<img src="painel/logo/lp/logo.png" alt="<?php echo $jsonConf->c_nome?>" width="400" class="logo img-fluid">

						</p>
				        <h2 class="text-shadow text-white title mt-xl-3 text-uppercase"><?php echo $landRow->land_titulo;?></h2>
			</div>
			
			<div class="col-xl-4 col-md-6 col-sm-12 mx-auto"> 
				<div class="card my-5">
					<div class="card-header">
						<h2 class="pb-2 text-center"><?php echo $landRow->land_titulo_form;?></h2>
						<p class="text-center lead p-0 m-0">Preencha os dados abaixo e receba nosso material exclusivo.</p>
					</div>
				<div class="card-body">
					<div class="bg-white form-land wow fadeInUp m-2">
							<form class="pt-2" id="contato">
								<div class="form-group">
									<input type="text" class="form-control" name="nome" id="nome" placeholder="Seu nome">
								</div>
								<div class="form-group">
									<input type="email" class="form-control" name="email" id="email" placeholder="Seu email">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="telefone" id="telefone" placeholder="Seu telefone">
								</div>
								<div class="form-group text-center">
									<span id="ReturnoContato">
										<button type="button" onclick="javascript:Send();" class="btn btn-lg btn-outline-primary w-100">Solicitar <i class="fa fa-angle-right"></i></button>
									</span>
								</div>
								<input type="hidden" name="destino" value="contato@contabexpress.com.br">
								<input type="hidden" name="pagina" value="[LD] Lucro Real">
								<p class="text-center lead text-small">*Fique tranquilo, nós também não gostamos de SPAM.</p>
							</form>


							<script type="text/javascript">
								function Send()
								{  
									$send = jQuery.noConflict(); 
									$send('#ReturnoContato').html('<div class="bg-primary p-3"><div class="spinner-border text-light" role="status"><span class="sr-only">Loading...</span></div></div>');
									$send.post('conteudo/contato.php', $send( "#contato" ).serialize() ,function(data){
										$send('#ReturnoContato').html(data);
										return false;
									});
									return false;
								}
							</script>

					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="conteudo p-0 wow fadeInRight">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="my-5">
					<h1 class="text-uppercase"><?php echo $landRow->land_titulo;?></h1>
				</div>
			</div>
			<div class="col-md-12">
				<div>
					<?php echo stripcslashes($landRow->land_descricao);?>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
    <div class="text-center">
	    <a class="scroll" href="conteudo/lucro-real#topo">
		   <i class="fas fa-angle-double-up"></i>
		</a>
	</div>
</section>

<footer class="my-4 py-5 wow fadeIn">

	<div class="footer-section" style="visibility: visible; animation-name: fadeIn;">
		<div class="container">
			<div class="row">
				<div class="col-md-4 offset-md-4">
					<div class="footer-about wow bounceInLeft" data-wow-delay="0.7" style="visibility: visible; animation-name: bounceInLeft;">
						<img src="painel/logo/principal/logo.png" alt="<?php echo $jsonConf->c_nome?>" class="img-fluid" width="150">
					</div><!-- .footer-about -->
				</div>	
				<?php if( !empty($__unidade->exibi['cli_cidade']) ){  ?>
				<div class="col-md-12 mt-4">
					<div class="footer-address wow bounceInUp" data-wow-delay="0.5" style="visibility: visible; animation-name: bounceInUp;">
						<h6 class="fa-title text-center">ContabExpress Unidade de <?php echo $__unidade->exibi['cli_cidade'] ?></h6>
						<div class="address-list text-center">
							<div class="single-address">
								<ul class="m-0 p-0">
									<?php if( !empty($__unidade->exibi['cli_telefone']) ){ ?><li><span><i class="fa fa-phone"></i></span> <?php echo $__unidade->exibi['cli_telefone'] ?></li><?php } ?>
									<?php if( !empty($__unidade->exibi['cli_celular']) ){ ?><li><span><i class="fa fa-whastapp"></i></span> <?php echo $__unidade->exibi['cli_celular'] ?></li><?php } ?>
									<?php if( !empty($__unidade->exibi['cli_email']) ){ ?><li><span><i class="fa fa-envelope"></i></span> <?php echo $__unidade->exibi['cli_email'] ?></li><?php } ?>

									<?php if( !empty($__unidade->exibi['cli_endereco']) ){ ?>
									<li>
										<span><i class="fa fa-map-marker"></i></span> <?php echo $__unidade->exibi['cli_endereco'] ?>, <?php echo $__unidade->exibi['cli_numero'] ?>
										<br>
										<?php echo $__unidade->exibi['cli_cidade'] ?> | <?php echo $__unidade->exibi['cli_estado'] ?>
									</li>
									<?php } ?>

								</ul>
							</div>
						</div><!-- .address-list -->
					</div><!-- .footer-address -->
				</div><!-- .col -->
				<?php } ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div>

</footer>

<div class="container">
	<div class="row">
		<div class="col-12 pb-4">
			<span class="copyright-text"><?php echo date('Y')?> (C) • Todos os direitos reservados à Grupo Dicon</span>
		</div>
	</div>
</div>

<script>


jQuery(function($){

	/* Mascara */
	jQuery("#telefone").mask("(99) 9999-9999");

	/* SCROOL */
	$(".scroll").click(function(e) {
		e.preventDefault();
		
		var position = $($(this).attr("href")).offset().top;

		console.log('Subindo...');

		$("body, html").animate({
			scrollTop: position
		} /* speed */ );
	});

});


</script>