<?php

date_default_timezone_set('America/Sao_Paulo');

/***************************/
/** BIBIOTECAS            **/
/***************************/
include('../painel/php/db.class.php');

include('assets/php/acentos.php');
include('assets/php/data.php');
include('assets/php/uf.php');
include('assets/php/tags.php');
include('assets/php/funcaoStringParaBusca.php');
include('assets/php/capa.php');
include('assets/php/scripts.php');
include('../assets/php/htaccess.php');
include('../assets/php/jsonConf.php');

/***************************/
/** DADOS DA LANDPAGE     **/
/***************************/
$land = new db();
$land->query("SELECT * FROM landpage WHERE land_url = '".$link[0]."'");
$land->execute();
$landRow = $land->object();
echo $land->rowCount();

/* ************************************************** */
/* UNIDADES */
/* ************************************************** */
/*
$__unidade = new db();
$__unidade->query( "SELECT * FROM cliente WHERE cli_url = '".$link[0]."'" );
$__unidade->execute();
$__unidadeRow = $__unidade->object();
*/
?>

<!DOCTYPE html>
<html lang="en" class="js">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta name="robot" content="All">
	<meta name="rating" content="general">
	<meta name="distribuition" content="global">
	<meta name="language" content="PT">
	<meta name="author" content="WebFreelancer"> 
	<meta name="robots" content="index,follow">

	<meta name="description" content="Saiba a importância de escolher um bom escritório de contabilidade especializada, para direcionar a situação econômico-financeira da sua empresa.">
	<meta name="keywords" content="">

	<meta property="og:title" content="<?php echo $jsonConf->c_nome.' '.$landRow->land_titulo;?>">
	<meta property="og:description" content="Saiba a importância de escolher um bom escritório de contabilidade especializada, para direcionar a situação econômico-financeira da sua empresa.">
	<meta property="og:image" content="painel/images/fotos-landpage/<?php echo $landRow->land_id;?>/g/<?php echo $landRow->land_capa;?>">

	<meta property="og:url" content="<?php echo $url.$link[0].'/'.$link[1];?>">
	<meta property="og:type" content="website" /> 
	<meta property="og:site_name" content="Grupo Dicon">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="500">
	<meta property="og:image:height" content="500">

	<base href="<?php echo $url.$link[0];?>">

	<!-- Fav Icon  -->
	<link rel="shortcut icon" href="<?php echo $url;?>images/ico/favicon.png">
	<link href="<?php echo $url;?>images/ico/favicon.png" rel="shortcut icon" type="image/x-icon">
	<link href="<?php echo $url;?>images/ico/favicon.png" rel="icon" type="image/x-icon">

	<!-- Site Title  -->
	<title><?php echo $jsonConf->c_nome.' | '.$landRow->land_titulo ?></title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

	<!-- WOW animation -->
	<link rel="stylesheet" href="conteudo/assets/wow/animate.css" />

	<!-- Style -->
	<link rel="stylesheet" href="conteudo/assets/css/style.css" />


	<style>
	.slider{
		background-image: url('painel/images/fotos-landpage/<?php echo $landRow->land_id;?>/g/<?php echo $landRow->land_capa;?>');
		background-size: cover;
		background-position: center;
	}
</style>



</head>
<body id="topo">

	<?php include('wireframes/'.$landRow->land_wireframe.'/#home.php'); ?>

<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- WOW
	================================================== -->
	<script src="<?php echo $url;?>assets/wow/wow.min.js" type="text/javascript"></script>
	<script>
		new WOW().init();
	</script>

</body>

</html>
