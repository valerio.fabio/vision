<form class="contact-form w-100 orcamento" name="contact-form" method="post">

    <div class="p-3 row">
        <div class="card-header col-12 mb-3"><h3 class="p-0 m-0 lead">Formulário de Orçamento</h3></div>
            
            <div class="form-group col-md-6">
                    <input type="text" name="nome" id="nome" class="form-control" required="required" placeholder="Seu nome">
            </div>
            <div class="form-group col-md-6">
                    <input type="email" name="email" id="email" class="form-control" required="required" placeholder="E-mail">
            </div>
            <div class="form-group col-md-6">
                    <input name="telefone" id="telefone" type="text" class="form-control" required="required" placeholder="Telefone">
            </div>  
            <div class="form-group col-md-6">
                    <input type="text" name="cidade" id="cidade" class="form-control" required="required" placeholder="Cidade e Estado">
            </div>       
            <div class="form-group col-12">
            <p class="p-0 m-0">
                Ramo(s) de atividade:
            </p>
                <span class=""><label><input type="checkbox" name="atividade[]" value="Serviço"> Serviço</label></span>
                <span class=""><label><input type="checkbox" name="atividade[]" value="Comércio"> Comércio</label></span>
                <span class=""><label><input type="checkbox" name="atividade[]" value="Indústria"> Indústria</label></span>
                <span class=""><label><input type="checkbox" name="atividade[]" value="Associação"> Associação</label></span>
            </div>      
            <div class="form-group col-12">
                <textarea name="mensagem" id="mensagem" required class="form-control " rows="4" placeholder="Sua mesagem"></textarea>
            </div>                        
        <div class="form-group col-12">
            <input type="hidden" name="pagina" value="orcamento">
            <button type="submit" name="submit" class="btn btn-primary btn-lg d-block w-100">Enviar</button>
        </div>
    </div>

</form> 
