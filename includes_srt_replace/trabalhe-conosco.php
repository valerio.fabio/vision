<form action="" method="post" class="row curriculo needs-validation" novalidate>

    <h2 class="col-lg-12 my-5 display-4">Informações Pessoais</h2>

    <div class="col-lg-12 my-2">
        Nome <span class="text-danger font-weight-bold">*</span>
        <div><input type="text" name="nome" maxlength="70" class="form-control required"></span></div>
    </div>

    <div class="col-lg-6 my-2">
        Município <span class="text-danger font-weight-bold">*</span>
        <div><input type="text" name="municipio" maxlength="20"class="form-control required"></div>
    </div>

    <div class="col-lg-6 my-2">
        Estado <span class="text-danger font-weight-bold">*</span>
        <div><input type="text" name="estado" maxlength="2" class="form-control"></div>
    </div>

    <div class="col-lg-6 my-2">
        Telefone <span class="text-danger font-weight-bold">*</span>
        <div><input type="text" name="fone" maxlength="40" class="form-control"></div>
    </div>
    
    <div class="col-lg-6 my-2">
        Telefone 2 <span class="text-danger font-weight-bold">*</span>
        <div><input type="text" name="fone2" maxlength="40" class="form-control"></div>
    </div>

    <div class="col-lg-12 my-2">
        E-mail
        <div><input type="text" name="email" maxlength="50"class="form-control">
        </div>
    </div>

    <div class="col-lg-4 my-2">
        Sexo <span class="text-danger font-weight-bold">*</span>
        <div>
        <select name="sexo" class="form-control  ">
                    <option value="">---</option>
                    <option value="FEMININO">FEMININO</option>
                    <option value="MASCULINO">MASCULINO</option>
                </select>
                </div>
    </div>

    <div class="col-lg-4 my-2">
        Estado Civil <span class="text-danger font-weight-bold">*</span>
        <div>
        <select name="estado_civil" class="form-control  ">
                    <option value="">---</option>
                    <option value="CASADO">CASADO</option>
                    <option value="DIVORCIADO">DIVORCIADO</option>
                    <option value="UNIÃO ESTÁVEL">UNIÃO ESTÁVEL</option>
                    <option value="DESQUITADO">DESQUITADO</option>
                    <option value="SOLTEIRO">SOLTEIRO</option>
                    <option value="VIÚVO">VIÚVO</option>
                </select>
                </div>
    </div>

    <div class="col-lg-4 my-2">
        Possui Filhos?
        <div>
        <select name="temfilhos" class="form-control ">
                    <option value="">---</option>
                    <option value="SIM">SIM</option>
                    <option value="NÃO">NÃO</option>
                </select>
                </div>
    </div>

    <div class="col-lg-4 my-2">
        Quantos Filhos?
        <div><input type="text" name="nr_filhos" maxlength="2" class="form-control"></div>
    </div>

    <div class="col-lg-4 my-2">
        Data de Nascimento <span class="text-danger font-weight-bold">*</span>
        <div><input type="date" name="datanas_cimento" class="form-control "></div>
    </div>

    <div class="col-lg-4 my-2">
        Possui Algum Tipo de Deficiência? <span class="text-danger font-weight-bold">*</span>
        <div><select name="deficiente" class="form-control  ">
                    <option value="">---</option>
                    <option value="SIM">SIM</option>
                    <option value="NÃO">NÃO</option>
                </select>
                </div>
    </div>

<h2 class="my-5 col-12 display-4">Área de interesse </h2>

<div class="col-lg-6 my-2">
        Seleciono área de atuação <span class="text-danger font-weight-bold">*</span>
        <select name="area_empresa" class="form-control ">
        <option value="">---</option>
            <option value="Departamento Pessoal">Departamento Pessoal</option>
            <option value="Fiscal">Fiscal</option>
            <option value="Departamento Legal">Departamento Legal</option>
            <option value="Tributário">Tributário</option>
            <option value="Contábil">Contábil</option>
            <option value="Estágio">Estágio</option>
        </select>
</div>

    <h2 class="my-5 col-12 display-4">Informações Acadêmicas</h2>
    <div class="col-lg-6 my-2">
        Possui Formação Acadêmica ?<br>
        <select name="descr_tipo_cursoa" class="form-control ">
            <option value="NÃO">NÃO</option>
            <option value="SIM">SIM</option>
        </select>

    </div>

    <div class="col-lg-6 my-2">
        Descrição do Nível do Curso<br>
        <select name="descr_nivel_a" class="form-control ">
                <option value="">---</option>
                <option value="ENSINO FUNDAMENTAL">ENSINO FUNDAMENTAL</option>
                <option value="ENSINO MEDIO">ENSINO MEDIO</option>
                <option value="TECNICO">TECNICO</option>
                <option value="SUPERIOR INCOMPLETO">SUPERIOR INCOMPLETO</option>
                <option value="SUPERIOR EM ANDAMENTO">SUPERIOR EM ANDAMENTO</option>
                <option value="SUPERIOR COMPLETO">SUPERIOR COMPLETO</option>
                <option value="MBA">MBA</option>
                <option value="MESTRADO">MESTRADO</option>
                <option value="DOUTORADO">DOUTORADO</option>
                <option value="PÓS GRADUAÇÃO">PÓS GRADUAÇÃO</option>
            </select>
    </div>

    <div class="col-lg-6 my-2">Nome do Curso<br>
    <input type="text" name="nome_curso_a" maxlength="35" class="form-control">
    </div>

    <div class="col-lg-6 my-2"> Data da conclusão do curso<br>
        <input type="date" name="curso_dt_concla" class="form-control">
    </div>

    <div class="col-lg-12">
    <hr class="my-3">
    </div>

    <div class="col-lg-6 my-2">Possui outra Formação Acadêmica ?<br>
        <select name="descr_tipo_curso_b" class="form-control ">
                <option value="NÃO">NÃO</option>
                <option value="SIM">SIM</option>
            </select>
    </div>
    <div class="col-lg-6 my-2">
        Descrição do Nível do Curso<br>
        <select name="descr_nivel_b" class="form-control ">
                <option value="">---</option>
                <option value="ENSINO FUNDAMENTAL">ENSINO FUNDAMENTAL</option>
                <option value="ENSINO MEDIO">ENSINO MEDIO</option>
                <option value="TECNICO">TECNICO</option>
                <option value="SUPERIOR INCOMPLETO">SUPERIOR INCOMPLETO</option>
                <option value="SUPERIOR EM ANDAMENTO">SUPERIOR EM ANDAMENTO</option>
                <option value="SUPERIOR COMPLETO">SUPERIOR COMPLETO</option>
                <option value="MBA">MBA</option>
                <option value="MESTRADO">MESTRADO</option>
                <option value="DOUTORADO">DOUTORADO</option>
            </select>
    </div>

    <div class="col-lg-6 my-2">
        Nome do Curso<br>
        <input type="text" name="nome_curso_b" class="form-control">
    </div>
    
    <div class="col-lg-6 my-2">
        Data da conclusão do curso<br>
        <input type="date" name="curso_dt_concl_b" class="form-control">
    </div>


    <hr class="col-12 my-3">

    <div class="col-lg-6 my-2">
        Possui Curso de Idioma ?<br>
        <select name="descr_tipo_curso_c" class="form-control ">
                <option value="NÃO">NÃO</option>
                <option value="SIM">SIM</option>
            </select>
    </div>
    <div class="col-lg-6 my-2">
        Descrição do Nível do Curso<br>
        <select name="descr_nivel_c" class="form-control ">
                <option value="">---</option>
                <option value="BASICO">BASICO</option>
                <option value="INTERMEDIARIO">INTERMEDIARIO</option>
                <option value="AVANCADO">AVANCADO</option>
                <option value="FLUENTE">FLUENTE</option>
            </select>
    </div>
    <div class="col-lg-6 my-2">
        Nome do Curso<br>
        <input type="text" name="nome_curso_c" class="form-control">
    </div>
    <div class="col-lg-6 my-2">
        Data da conclusão do curso<br>
        <input type="date" name="curso_dt_conclc" class="form-control">
    </div>

    <hr class="col-12 my-3">

    <div class="col-lg-6 my-2">
        Possui Curso Extracurricular ?<br>
        <select name="descr_tipo_curso_d" class="form-control ">
                <option value="NÃO">NÃO</option>
                <option value="SIM">SIM</option>
            </select>
    </div>
    <div class="col-lg-6 my-2">
        Descrição do Nível do Curso<br>
        <select name="descr_nivel_d" class="form-control ">
                <option value="">---</option>
                <option value="COMPLETO">COMPLETO</option>
                <option value="INCOMPLETO">INCOMPLETO</option>
        </select>
    </div>
    <div class="col-lg-6 my-2">
        Nome do Curso<br>
        <span class="nome_curso_d"><input type="text" name="nome_curso_d" class="form-control"></span>
    </div>
    <div class="col-lg-6 my-2">
        Data da conclusão do curso<br>
        <span class="curso_dtcon_cld"><input type="date" name="curso_dtcon_cld" class="form-control"></span>
    </div>

    <hr class="col-12 my-3">

    <div class="col-lg-6 my-2">
        Possui Mais Cursos ?<br>
        <select name="descr_tipo_curso_e" class="form-control ">
                <option value="NÃO">NÃO</option>
                <option value="SIM">SIM</option>
            </select>
    </div>
    <div class="col-lg-6 my-2">
        Descrição do Nível do Curso<br>
        <select name="descr_nivel_e" class="form-control ">
                <option value="">---</option>
                <option value="COMPLETO">COMPLETO</option>
                <option value="INCOMPLETO">INCOMPLETO</option>
            </select>
    </div>
    <div class="col-lg-6 my-2">
        Nome do Curso<br>
        <span class="nome_curso_e"><input type="text" name="nome_curso_e" class="form-control"></span>
    </div>
    <div class="col-lg-6 my-2">
        Data da conclusão do curso<br>
        <span class="curso_dt_concl_e"><input type="date" name="curso_dt_concl_e" class="form-control"></span>
    </div>

    <h2 class="my-5 col-12 display-4">Informações Profissionais</h2>
    <div class="col-lg-6 my-2">
        Data da Admissão<br>
        <span class="hist_admissao_a"><input type="date" name="hist_admissao_a" class="form-control"></span>
    </div>

    <div class="col-lg-6 my-2">
        Data de Demissão<br>
        <span class="hist_demissao_a"><input type="date" name="hist_demissao_a" class="form-control"></span>
    </div>

    <div class="col-lg-6 my-2">
        Nome da Empresa<br>
        <span class="hist_empresaa"><input type="text" name="hist_empresaa" size="40"
                class="form-control"></span>
    </div>

    <div class="col-lg-6 my-2">
        Função na Empresa<br>
        <span class="hist_funcao_a"><input type="text" name="hist_funcao_a" class="form-control"></span>
    </div>

    <div class="col-lg-12 my-2">
        Atividades Exercidas na Empresa<br>
        <textarea name="hist_ativ_memo_a" id="" cols="30" rows="10" class="form-control"></textarea>
    </div>

    <hr class="col-12 my-3">

    <div class="col-lg-6 my-2">
        Data da Admissão<br>
        <span class="hist_admissao_b"><input type="date" name="hist_admissao_b" class="form-control"></span>
    </div>

    <div class="col-lg-6 my-2">
        Data de Demissão<br>
        <span class="hist_demissao_b"><input type="date" name="hist_demissao_b" class="form-control"></span>
    </div>

    <div class="col-lg-6 my-2">
        Nome da Empresa<br>
        <span class="hist_empresa_b"><input type="text" name="hist_empresa_b" size="40"
                class="form-control"></span>
    </div>

    <div class="col-lg-6 my-2">
        Função na Empresa<br>
        <span class="hist_funcao_b"><input type="text" name="hist_funcao_b" class="form-control"></span>
    </div>


    <div class="col-lg-12 my-2">
        Atividades Exercidas na Empresa<br>
        <textarea name="hist_ativ_memo_b" id="" cols="30" rows="10" class="form-control"></textarea>
    </div>

    <div class="col-lg-12 my-2">
            <input type="hidden" name="pagina" value="Currículo">
            <button class="btn btn-primary" type="submit">Enviar</button>
    </div>
        
</form>



