<?php

$visSQL = "SELECT pag_id, pag_titulo, pag_capa FROM paginas WHERE pag_id = '".$link[3]."'";
$vis = new db();
$vis->query( $visSQL );
$vis->execute();
$row = $vis->object();


/*Imagem*/
$pastaId = 'images/fotos-paginas/'.$link[3];
$pasta = $pastaId.'/p/';
$arquivos = glob("$pasta{*.jpg,*.png,*.gif}", GLOB_BRACE);

?>

<link href="<?php echo $url ?>assets/fileinput-master/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<link href="<?php echo $url ?>assets/fileinput-master/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>

<script src="<?php echo $url ?>assets/fileinput-master/js/plugins/sortable.js" type="text/javascript"></script>
<script src="<?php echo $url ?>assets/fileinput-master/js/fileinput.js" type="text/javascript"></script>
<script src="<?php echo $url ?>assets/fileinput-master/js/locales/pt-BR.js" type="text/javascript"></script>
<script src="<?php echo $url ?>assets/fileinput-master/themes/fas/theme.js" type="text/javascript"></script>
<script src="<?php echo $url ?>assets/fileinput-master/js/plugins/piexif.js" type="text/javascript"></script>

<div class="d-flex">

  <div class="">
    <a class="btn btn-warning" href="<?php echo $url?>!/<?php echo $link[1]?>/editar/<?php echo $link[3]?>">Voltar</a>
    <a class="btn btn-primary" href="<?php echo $url?>!/<?php echo $link[1]?>/ordenar/<?php echo $link[3]?>">Ordenar sequencia das fotos</a>
    <a class="btn btn-success" href="<?php echo $url?>!/<?php echo $link[1]?>/cadastro">Novo cadastro</a>
    <a class="btn btn-danger" href="<?php echo $url?>!/<?php echo $link[1]?>/visualizar">Finalizar</a>
  </div>
  <div class="flex-grow-1 mt-1">
    <div class="stepwizard m-0">
      <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
          <div class="btn btn-secondary btn-circulo">
            <i class="fas fa-info-circle"></i>
          </div>
          <p>Informa&ccedil;&otilde;es</p>
        </div>
        <div class="stepwizard-step">
          <div class="btn btn-success btn-circulo">
            <i class="fas fa-images"></i>
          </div>
          <p>Insirir Imagens</p>
        </div>
        <div class="stepwizard-step">
          <div class="btn btn-secondary btn-circulo">
            <i class="fas fa-check-double"></i>
          </div>
          <p>Finalizar</p>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
/*configuração*/


$__configSql = "SELECT pag_galeria FROM paginas WHERE pag_id = '".$link[3]."'";

$__config = new db();     
$__config->query($__configSql);
$__config->execute();
$result__config = $__config->object();

$JsonGal = json_decode($result__config->pag_galeria);

//echo $JsonGal->c_tamanho_image;

/*  \configuracao */
?>

<div class="wireframes_galeria mb-4 card">
  <div class="card-body">
    <h2>Wireframes Galeria*</h2> <span class="label label-danger">Modelo de diagrama&ccedil;&atilde;o das imagens/fotos</span>


    <form action="" id="cadastro">
      <hr>
      <div class="row">
       <div class="col-md-4">
         <label for="">Container</label>
         <select name="json[container]" class="custom-select">
           <option value="container" <?php if( ($JsonGal->json->container == 'container') OR empty($JsonGal->json->container) ){ echo 'selected';} ?>>Container</option>
           <option value="container-fluid" <?php if( $JsonGal->json->container == 'container-fluid' ){ echo 'selected';} ?>>Container - Fluid (100%)</option>
         </select>
       </div>
       <div class="col-md-4">
         <label for="">Colunas</label>
         <select name="json[col]" class="custom-select">
           <option <?php if( $JsonGal->json->col == 'col-12' ){ echo 'selected';} ?> value="col-12">12 Coluna</option>
           <option <?php if( $JsonGal->json->col == 'col-2' ){ echo 'selected';} ?> value="col-6">6 Colunas</option>
           <option <?php if( $JsonGal->json->col == 'col-4' ){ echo 'selected';} ?> value="col-4">4 Colunas</option>
           <option <?php if( ($JsonGal->json->col == 'col-3') OR empty($JsonGal->json->col) ){ echo 'selected';} ?> value="col-3">3 Colunas</option>
           <option <?php if( $JsonGal->json->col == 'col-2' ){ echo 'selected';} ?> value="col-2">2 Colunas</option>
           <option <?php if( $JsonGal->json->col == 'col-1' ){ echo 'selected';} ?> value="col-1">1 Colunas</option>
         </select>
       </div>
       <div class="col-md-4">
         <label for="">Efeitos</label>
         <select name="json[efeito]" class="custom-select">
           <option value="" selected>Nenhum</option>
         </select>
       </div>
     </div>
     <hr>
     <div class="row">
       <?php

       $pastaGaleria = 'configuracao/wireframes/galeria/';
       $diretorioGaleria = dir($pastaGaleria);

       while($arquivoGaleria = $diretorioGaleria -> read()){
        $dirNumerGal = $pastaGaleria.$arquivoGaleria.'/';

        $arquivosGaleria = glob("$dirNumerGal{*.jpg,*.png,*.gif}", GLOB_BRACE);

        foreach($arquivosGaleria as $img){

          $modelo_wiri = @end( @explode('/', $img) );
          $modelo_wiri = @current( @explode('.', $modelo_wiri) );
          $modelo_wiri = @end( @explode('-', $modelo_wiri) );

          ?>
          <div class="col-lg-3 col-md-6 col-sm-6 text-sm-center p-3">
            <div class="btn btn-secondary">
              <label>
               <input type="radio" <?php if( $modelo_wiri == '1' ){ echo "checked"; } ?> name="galeria_wireframes_id" value="<?php echo $modelo_wiri; ?>">
               VER #<?php echo $modelo_wiri; ?>
               <hr>
               <img  class="img-fluid" data-toggle="tooltip" data-placement="bottom" title="MODELO <?php echo $modelo_wiri; ?>" src="<?php echo $url.$img; ?>" >
             </label>
           </div>
         </div>
         <?php
         $checked = '';
       }
     }
     $diretorioGaleria -> close();

     ?>
     <div class="col-12 my-4">
      <input type="hidden" name="id" value="<?php echo $link[3] ?>">
      <button type="button" id="salvar" class="btn btn-lg btn-success w-100">Salvar configuração</button>
      <span id="result"></span>
    </div>
  </div>
</form>

<script>
  jQuery(document).ready(function() {
    jQuery('#salvar').click(function() {

      var dados = jQuery('#cadastro').serialize();
      var registro = '<?php echo $url ?>paginas/registro-json-galeria.php';

      /*Efeito subir e aprecer*/
      jQuery("#salvar").fadeToggle("slow");
      setTimeout(function() { jQuery('#salvar').fadeIn('show');}, 5000);

      jQuery.ajax({
        url : registro,
        type : 'post',
        data : dados,
        beforeSend : function(){
         jQuery("#result").html("<p class=\"p-3 badge-warning\">ENVIANDO...</p>");
       }
     })
      .done(function(msg){
        jQuery("#result").html(msg);
      })
      .fail(function(jqXHR, textStatus, msg){
        jQuery("#result").html(msg);
      }); 

    });
  });
</script>

</div>
</div>


<div class="d-flex align-items-center">
  <h2>Fotos </h2> <h4 class="ml-4"><strong class="badge badge-pill badge-dark"><?php echo $row->pag_titulo?></strong></h4>
</div>
<div class="card">
  <div class="card-body">
    <form action="<?php echo $url.'!/'.$link[1].'/'.$link[2].'/'.$link[3]?>/enviar" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <div class="file-loading">
          <input id="file-5" name="file[]" class="file" type="file" multiple data-preview-file-type="any" data-upload-url="<?php echo $url ?>paginas/upload-paginas.php?pid=<?php echo $link[3]?>" data-theme="fas">
        </div>
      </div>
    </form>
  </div>
</div>


<!--Thumb-->    
<div class="card my-3">
  <div class="card-body">
    <div class="row">

      <?php 

      if( !empty(count($arquivos)) && empty($row->pag_capa) ){
        echo '<div class="col-md-12"><p class="mb-4 p-3 badge-danger"><b>Atenção:</b> necessário selecionar uma imagem como capa!!!<p/></div>';
      }
      else{
        echo '<div class="col-md-12"><p class="mb-4 p-3 badge-info">Galeria de imagens! <b>[ '.count($arquivos).' ]</b><p/></div>';
      }
      ?>

      <?php

      foreach($arquivos as $img){
       $nomeImagem = '/g/'.@end( @explode('/', $img) );
       ?>
       <div class="col-sm-6 col-md-2 mb-4">
        <div class="card">
          <div class="card-body badge-light">
            <a href="<?php echo $url.$pastaId.$nomeImagem; ?>" class="thumbnail group3" target="_new">
              <div class="capa" style="background-image:url('<?php echo $url.$img;?>'); height:100px;"></div>
            </a>
            <div class="text-center">
              <?php 
              $nameImagem = @end( @explode('/', $img) );
              if( $row->pag_capa != $nameImagem ){
                ?>
                <hr>
                <a href="<?php echo $url?>!/<?php echo $link[1]?>/deleta-imagem/<?php echo $link[3]?>/<?php echo $img?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                <a href="<?php echo $url?>!/<?php echo $link[1]?>/capa-imagem/<?php echo $link[3]?>/<?php echo $img?>" class="btn btn-success"><i class="fas fa-check"></i></a>
              <?php  }else{ ?>
                <hr>
                <a class="btn btn-info"><i class="fas fa-check"></i></a>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>

    <div class="clearfix"></div>

    <div class="container-fluid">
      <hr>
      <div class="row">
       <div class="col-md-3 col-sm-3 text-danger"> <i class="fa fa-times"></i> Deletar</div>
       <div class="col-md-3 col-sm-3 text-success"> <i class="fa fa-pencil"></i> Definir como capa</div>
       <div class="col-md-3 col-sm-3 text-primary"> <i class="fa fa-eye"></i> Imagem capa</div>
       <div class="col-md-3 col-sm-3 text-info"> <i class="fa fa-pencil-square-o"></i> Legenda e Link</div>
     </div>
     <hr>
   </div>

 </div>
</div>



<script>

  $("#file-5").fileinput({
    theme: 'fas',
    language: 'pt-BR',
    uploadUrl:  "upload-paginas.php",
    allowedFileExtensions: ['jpg', 'png', 'gif'],
    autoReplace: true,
    maxFileCount: 10,
    uploadAsync: false
  });

  $("#file-5").on('filebatchuploadsuccess', function(event, files, extra){
    window.location.href = '<?php echo $url.'!/'.$link[1].'/'.$link[2].'/'.$link[3]?>';
  });

</script>
