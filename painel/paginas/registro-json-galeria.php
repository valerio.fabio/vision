<hr>
<code class="badge-danger p-2 my-5">
	<?php
	$edi = json_encode($_POST);
	echo $edi;

	include('../php/db.class.php');
	include('../php/htaccess.php');
	include('../includes/#notify.php');

	$sql = "UPDATE paginas SET pag_galeria = :pag_galeria WHERE pag_id = :pag_id";
	$db = new db();
	$db->query($sql);

	$db->bind(":pag_id", $_POST['id']);
	$db->bind(":pag_galeria", $edi);

	/* Editado sucesso */
	if($db->execute()){
		echo notify('success');
	} else{
		echo notify('danger');
	}
	?>
</code>

