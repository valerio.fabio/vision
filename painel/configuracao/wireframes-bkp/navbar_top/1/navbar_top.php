<div class="bar p-3 wow bounceInDown">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-xl-8 col-lg-7 col-md-6 d-md-flex">
					<div class="flex-fill btn-nav-top d-none d-md-block">
                        <a href="#" target="new" class="btn btn-info btn-sm"><i class="far fa-calendar-alt fa-fx"></i> <span>Agendar uma reunião</span></a>
                        <span class="mx-2">|</span>
						<a href="#" target="new" class="btn btn-outline-info btn-sm"><i class="fas fa-money-check fa-fx"></i> <span>Lucro Presumido</span></a>
                        <a href="#" target="new" class="btn btn-outline-info btn-sm"><i class="fas fa-money-check-alt fa-fx"></i> <span>Lucro Real</span></a>
                        <a href="#" target="new" class="btn btn-outline-info btn-sm"><i class="fas fa-money-check-alt fa-fx"></i> <span>Simples Nacional</span></a>
					</div>
			</div>

			<div class="col-xl-4 col-lg-5 col-md-6 text-md-right">
				<!--<a href="fale-conosco" class="btn btn-success"> Fale Conosco </a>-->
<a href="#" data-toggle="tooltip" data-placement="bottom" title="Português"><img height="35" class="<?php if($link[0] == 'en'){ ?>hvr-bounce-in filtro-cinza<?php } ?>" src="<?php echo $url ?>/assets/images/bandeiras/pt-br.svg" alt=""></a>
				<a href="#" data-toggle="tooltip" data-placement="bottom" title="English"><img height="35" class="<?php if($link[0] != 'en'){ ?>hvr-bounce-in filtro-cinza<?php } ?>" src="<?php echo $url ?>/assets/images/bandeiras/en.svg" alt=""></a>

				<span class="mx-3">|</span>

				[[midias-sociais]]
			</div>
		</div>
	</div>
</div>