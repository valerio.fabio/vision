<?php 

function imagem($url, $images, $sizeCol, $listar){
	
	switch ($sizeCol) {
		case 'col-2': $sizeimg = 'p/'; break;
		case 'col-3':
		case 'col-4': $sizeimg = 'm/'; break;
		case 'col-6': 
		case 'col-12': $sizeimg = 'g/'; break;
		
		default: break;
	}
	return $url.str_replace(array('../', $listar.'/'), array('', $sizeimg), $images);
}


function aimagem($url, $images, $listar){
	return $url.str_replace(array('../', $listar.'/'), array('', 'g/'), $images);
}

// print_r($_GET);

$id = $_GET['pid'];
$pasta = '../../../../images/fotos-paginas/';

include('../../../../php/db.class.php');
include('../../../../../assets/php/function-bootstrap-row.php');


$idPage = new db();
$idPage->query("SELECT pag_id, pag_capa, pag_galeria FROM paginas WHERE pag_id = '".$id."'");
$idPage->execute();
$rowIdPage = $idPage->object();

//  ========================= 
// CAPA 
$lista_pasta['p'] = $pasta.$rowIdPage->pag_id.'/p/';
$lista_pasta['m'] = $pasta.$rowIdPage->pag_id.'/m/';
$lista_pasta['g'] = $pasta.$rowIdPage->pag_id.'/g/';

$capa = $lista_pasta['m'].$rowIdPage->pag_capa;

$listar = 'm';

//  ========================= 
// Json Configuração

$json = json_decode($rowIdPage->pag_galeria);
$jImagens = $json->json;
// print_r($jImagens);

//* ========================== 
//* verificar se está ordenado 
//* ========================== 

$ordenacao_imagens = new db();
$ordenacao_imagens->query( "SELECT * 
	FROM ordenar 
	WHERE ord_tabela = 'paginas' 
	AND ord_pid = '{$__vis->exibi['pag_id']}' 
	ORDER BY ord_lista ASC" ); 
$ordenacao_imagens->execute();


if( !empty( $ordenacao_imagens->rowCount() ) ){ 
	foreach( $ordenacao_imagens->row() as $row ){
		// echo "<p>Orgainzador</p>";
		$arquivos[] = $pastas.$row['ord_image'];
	}
}else{
		// echo "<p>pasta</p>";
	$arquivos = glob($lista_pasta[$listar]."{*.jpg,*.png,*.gif}", GLOB_BRACE);
}

$qtdeImages = count($arquivos);

//* ========================== 
//* json de configuração 
//* ========================== 

$__configSql = "SELECT pag_galeria FROM paginas WHERE pag_id = '".$link[3]."'";

$__config = new db();     
$__config->query($__configSql);
$__config->execute();
$result__config = $__config->object();

$JsonGal = json_decode($result__config->pag_galeria);


// Prints
// print_r($lista_pasta);

?>

<script>
.card-columns {
    -webkit-column-count: 2;
    -moz-column-count: 2;
    column-count: 2;
    -webkit-column-gap: 1.25rem;
    -moz-column-gap: 1.25rem;
    column-gap: 1.25rem;
}

</script>

<section id="galeria" class="py-4 wow fadeIn">

	<div class="<?php echo $jImagens->container ?>">
		<div class="row" id="galeria_thumb">
			<div class="col-12 my-3">
				<h3>Galeria</h3>
			</div>
		</div>

		<!-- <div class="row d-flex justify-content-center" id="galeria_thumb">
			<?php 
			   		if( !empty($arquivos) ){
						foreach( $arquivos as $files ){
							if( file_exists($files) ){
							?>
							<div class="text-center <?php echo coluna_row($jImagens->col)?> my-2">
									<a data-toggle="lightbox" data-gallery="example-gallery" href="<?php echo aimagem('painel/', $files, $listar) ?>">
										<img class="img-fluid <?php echo $jImagens->efeito ?>" src="<?php echo imagem('painel/', $files, $jImagens->col, $listar) ?>" alt="">
									</a>
							</div>
							<?php
							}
						}  
					}
			   ?>
		</div> -->
		
		<div class="card-columns" id="galeria_thumb">
			<?php 
			   		if( !empty($arquivos) ){
						foreach( $arquivos as $files ){
							if( file_exists($files) ){
							?>
							<div class="card">
									<a data-toggle="lightbox" data-gallery="example-gallery" href="<?php echo aimagem('painel/', $files, $listar) ?>">
										<img class="img-fluid <?php echo $jImagens->efeito ?>" src="<?php echo imagem('painel/', $files, $jImagens->col, $listar) ?>" alt="">
									</a>
							</div>
							<?php
							}
						}  
					}
			   ?>
		</div>

	</div>

</section>