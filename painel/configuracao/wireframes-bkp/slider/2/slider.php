<?php

$slider = new db();
$slider->query( "SELECT *
 FROM     banner
 ORDER BY ban_order ASC" );
//$slider->execute();

if( !empty($slider->execute() )){

  ?>

  <div id="carousel-slider" class="carousel slide wow bounceInUp carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php for($i=0; $i<$slider->num_linha; $i++){ ?>
        <li data-target="#carousel-slider" data-slide-to="<?php echo $i; ?>" <?php if($i == '0' ){ echo 'class="active"'; }?>></li>
      <?php } ?>
    </ol>

    <div class="carousel-inner" role="listbox">

      <?php
      $aux = 0;
      foreach( $slider->row() AS $row ){
        ?>
        <div class="carousel-item <?php if($aux == '0' ){ echo 'active'; }?>"  role="listbox"  style="background-image: url('painel/images/fotos-banner/<?php echo $row['ban_id']; ?>/g/<?php echo $row['ban_capa']; ?>'); background-position:<?php echo $row['ban_posicao']; ?>!important;">

            <div class="container">
              <div class="row container-slide d-flex align-items-center">
                <div class="col-lg-5">
                  <?php if( !empty($row['ban_titulo']) ){?><h2 class="font]"><b><?php echo $row['ban_titulo']; ?></b></h2><?php }?>
                  <?php if( !empty($row['ban_descricao']) ){?><p><?php echo $row['ban_descricao']; ?></p><?php }?>
                  <?php if( !empty($row['ban_link']) ){?>
                    <a href="<?php echo $row['ban_link']; ?>" title="<?php echo $row['ban_titulo']; ?>" class="font-italic btn btn-outline-light btn-lg">veja mais</a>
                    <?php } ?>
                  </div>
                  <?php if( !empty($row['ban_include']) ){ ?>
                    <div class="col-lg-5 align-self-center text-slider">
                      <?php include( 'pages/'.$row['ban_include'] );?>
                    </div>
                  <?php } ?>
                </div>
              </div>


          </div>
          <?php
          $aux++;
        }
        ?>

        <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

      </div>

    </div>

    <?php } ?>