<?php
include('assets/php/jsonConf.php');
?>

<section class="py-4 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-12">
				[[texto-2]]
				<form class="news" method="post">
					<label class="sr-only" for="inputNews">Seu e-mail</label>
					<div class="input-group mb-2">
						<input type="mail" name="email" class="form-control form-control-lg" id="inputNews" placeholder="Seu e-mail" required>
						<div class="input-group-prepend">
							<div class="input-group-text badge-success">
								<input type="hidden" value="newsletter" name="pagina">
								<button type="submit" class="btn p-0">
									<i class="fas fa-check text-white"></i>
								</button>
							</div>
						</div>
					</div>
					<small>*Fique tranquilo, nós também não gostamos de SPAM.</small>
				</form>
			</div>
		</div>
	</div>
</section>

<footer class="">
	<div class="<?php echo $jsonConf->c_footer_largura?>">
		<div class="row">
			<div class="col-xl-2 col-12 align-self-center text-center">
				<img src="<?php echo $url ?>painel/logo/footer/logo.png" class="logo-footer"	alt="<?php echo $jsonConf->c_nome ?>">
			</div>
			<div class="col-xl-3 col-lg-3 col-md-6">
				<ul class="list-group list-group-flush">
					<li class="list-group-item disabled"><h5 class="lead">Entre em contato</h5></li>
					<li class="list-group-item">(12) 3625-5433</li>
					<li class="list-group-item">atendimento@diconcontabilidade.com.br</li>
					<li class="list-group-item px-0">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.781094417479!2d-45.564722384485414!3d-23.031808448135568!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ccf902ab57eaef%3A0xe2c43fc612dbe5b3!2sDicon+Contabilidade!5e0!3m2!1spt-BR!2sbr!4v1562011008167!5m2!1spt-BR!2sbr" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
					</li>
				</ul>
			</div>
			<div class="col-xl-2 col-lg-3 col-md-6">
				<ul class="list-group list-group-flush">
					<li class="list-group-item disabled"><h5 class="lead">Acessos</h5></li>
					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>contato">Fale Conosco</a></li>
					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>trabalhe-conosco">Trabalhe Conosco</a></li>
					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>links-uteis">Link Úteis</a></li>
					<li class="list-group-item"><a class="text-white" href="#">Agendar uma reunião</a></li>
					<li class="list-group-item"><a class="text-white" href="#">Solicitar Orçamento</a></li>
					<li class="list-group-item"><a class="text-white" href="#">Abertura de Empresa</a></li>
				</ul>
			</div>
			<div class="col-xl-2 col-lg-3 col-md-6">
				<ul class="list-group list-group-flush">
					<li class="list-group-item disabled"><h5 class="lead">Acessos</h5></li>
					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>contato">Fale Conosco</a></li>
					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>trabalhe-conosco">Trabalhe Conosco</a></li>
					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>links-uteis">Link Úteis</a></li>
					<li class="list-group-item"><a class="text-white" href="#">Agendar uma reunião</a></li>
					<li class="list-group-item"><a class="text-white" href="#">Solicitar Orçamento</a></li>
					<li class="list-group-item"><a class="text-white" href="#">Abertura de Empresa</a></li>
				</ul>
			</div>
			<div class="col-xl-2 col-lg-3 col-md-6 text-center">
				<div id="fb-root"></div>
				<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.3&appId=156780015100013&autoLogAppEvents=1"></script>
				<div class="fb-page" data-href="https://www.facebook.com/DiconContabilidadeTaubate/" data-tabs="timeline" data-width="300" data-height="315" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/DiconContabilidadeTaubate/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/DiconContabilidadeTaubate/">Dicon Contabilidade</a></blockquote></div>
			</div>
		</div>

	</div>
</footer>