<div class="direitos">
	<div class="container">
		<div class="row d-flex align-content-center">
 			<div class="col-sm-6  text-sm-left text-xs-center align-self-center"> <i class="far fa-copyright"></i> <?php echodate('Y')?> &bull; Todos os direitos reservados a <b><?php echo $jsonConf->c_nome  ?></b></div>
			<div class="col-sm-3 text-sm-right text-xs-center align-self-center">
				Feito com muito <i class="fas fa-heart text-danger"></i> em Taubat&eacute;
			</div>
			<div class="col-sm-3 text-sm-right text-xs-center">
				<a href="<?php echo $conf->exibi['c_url']; ?>" target="new" data-toggle="tooltip" data-placement="top" title="Gostou? A gente que fez!">
					<img id="wf" src="<?php echo $url?>painel/images/estrutura/logo.svg" alt="Webfreelancer">
				</a>
			</div>
		</div>
	</div>
</div>
