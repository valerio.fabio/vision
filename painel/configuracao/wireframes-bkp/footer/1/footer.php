<?php
include('assets/php/jsonConf.php');
?>
<footer class="">
	<div class="<?php echo $jsonConf->c_footer_largura?>">
		<div class="row">
			<div class="col-lg-6">
                <div class="mb-3"><img src="<?php echo $url ?>painel/logo/footer/logo.png" class="logo-footer" alt="<?php echo $jsonConf->c_nome ?>"></div>
				[[texto-3]]
			</div>
			<div class="col-lg-6">
					[[texto-2]]
				<form class="news" method="post">
					<label class="sr-only" for="inputNews">Seu e-mail</label>
					<div class="input-group mb-2">
						<input type="mail" name="email" class="form-control form-control-lg" id="inputNews" placeholder="Seu e-mail" required>
						<div class="input-group-prepend">
							<div class="input-group-text">
								<input type="hidden" value="newsletter" name="pagina">
								<button type="submit" class="btn btn-link p-0">
									<i class="fas fa-check"></i>
								</button>
							</div>
						</div>
					</div>
					<small>*Fique tranquilo, nós também não gostamos de SPAM.</small>
				</form>

			</div>
		</div>
	</div>
</footer>
