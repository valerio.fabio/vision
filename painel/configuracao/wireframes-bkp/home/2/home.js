/* Menu Home Ativo */
if( menuHome != '' ){
	var colmenuHome        = "";
	var valorRetornadoHome = menuHome ;
	var objCat             = JSON.parse(valorRetornadoHome);
	objCat.forEach(function(c, index){

					colmenuHome +=  '<div class="box-section my-4" id="inicial-'+c.menu_id+'">';
							colmenuHome +=  '<h1 class="titulo-destaque-home display-4">'+c.menu_titulo+'</h1>';
						colmenuHome +=  '<div class="container p-0 home-conteudo" id="home-conteudo-'+c.menu_id+'">';
						colmenuHome +=  '<div class="row p-0 m-0">';

						/*--------------------------------------------------------------------------------------------------*/

						/* Conteudo Home Ativo */
						if( destaqueHome != '' ){
							var objContH  = JSON.parse(destaqueHome);
							var coldestaqueHome = '';
							var menu = c.menu_id;
							objContH.forEach(function(c, index){
								c.conteudo.forEach(function(a, index){
									if( menu == a.menu_menu_id ){
										colmenuHome += '<div class="grid col-lg-4 col-md-6 my-3">';
											colmenuHome += '<a href="'+a.pag_link+'"" class="text-dark">';
												colmenuHome += '<div class="card card-box-home">';
													//colmenuHome += '<div class="p-3">';
													colmenuHome += '<picture class="lozad">';
														colmenuHome += '<img data-src="'+a.pag_capa+'" alt="'+a.pag_titulo+'" class="w-100 capa card-img-top lozad">';
													colmenuHome += '</picture>';
													//colmenuHome += '</div>';
													colmenuHome += '<div class="card-body">';
														colmenuHome += '<h2 class="lead font-weight-bolder">'+a.pag_titulo+'</h2>';
														colmenuHome += '<p class="text-justify font-weight-light">'+a.pag_mini_descricao+'</p>';
													colmenuHome += '</div>';
												colmenuHome += '</div>';
											colmenuHome += '</a>';
										colmenuHome += '</div>';
									}
								});
							});
						}

						/*--------------------------------------------------------------------------------------------------*/

						colmenuHome +=  '</div>';
						colmenuHome +=  '</div>';
						colmenuHome +=  '<div class="col-12 mt-5 text-center">';
							colmenuHome +=  '<a href="'+c.menu_link+'" class="btn veja-mais">Veja mais</a>';
						colmenuHome +=  '</div>';
					colmenuHome +=  '</div>';

	});
}

/* PRINT */
if( colmenuHome != '' ){
	$("#pagina-index-home").append(colmenuHome);
}

	$(window).scroll(function() {
        if ($(this).scrollTop() > 150) { 
			//console.log('foi');


            var grid = $('.home-conteudo').masonry({
                // options...
				itemSelector: '.grid',
				percentPosition: true,
                //columnWidth: 200
				});

			// layout Masonry after each image loads
			/*grid.imagesLoaded().progress( function() {
				grid.masonry('layout');
			});*/
				

        }

 });

