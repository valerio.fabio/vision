
<div class="container wow bounceInDown" data-wow-delay=".8s">
	<div class="row my-4">
		<div class="col-12  d-flex align-items-center">
			<h1 id="menu_titulo"></h1>
			<div class=" ml-3 lead" id="menu_mini_texto"></div>
		</div>
		<?php
		if( !empty($link[1]) ){
			?>
		<div class="col-12">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo $url ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:history.go(-1)">Voltar</a></li>
				</ol>
			</nav>
		</div>
			<?php
		}
		?>
		<div class="col-12">
			<p class="pt-3" id="menu_descricao"></p>
		</div>
	</div>
</div>

<main id="pagina" class="mb-4">
	
	<div class="container">
		<div class="row">
			<!-- News 2 -->
			<div class="col-12 mb-3">
				<form class="card px-5 py-4 w-100 newsblog" method="post">
					<div class="row d-flex align-items-center">
						<div class="col-lg-6">
							<p class="font-weight-bold">Entre para nossa lista e receba conteúdos exclusivos e com prioridade</p>
						</div>
						<div class="col-lg-6">
							<div class="input-group mb-2">
								<input type="mail" name="email" class="form-control form-control-lg" id="inputNews" placeholder="Seu e-mail">
								<div class="input-group-prepend">
									<div class="input-group-text bg-primary">
										<input type="hidden" name="pagina" value="news-blog">
										<button type="submit" class="btn btn-link p-0 text-white">Cadastrar email</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>

			<a name="paginas"></a>

			<?php
			/* BUSCA */
			if( !empty( $_GET[q] ) ){
				?>
				<div class="col-12 mb-1">
					<div class="card badge-primary">
						<div class="card-body">
							<b class="lead">Busca por | </b> <?php echo $_GET['q'] ?>
						</div>
					</div>
				</div>
				<?php
			}
			?>

			<!-- Blog -->
			<div class="col-lg-9 postagem order-2 order-lg-1">
				<div class="row conteudo-blog mt-4"></div>

				<nav aria-label="Page navigation example">
					<ul class="pagination d-flex justify-content-center"></ul>
				</nav>
			</div>
			
			<!--  Menu Navegação -->
			<div class="col-lg-3 order-1 order-lg-2 mt-4">
				<p class="lead">O que procura?</p>
				<div class="card p-2">
					<form class="buscablog" method="post">
						<input type="text" id="q" name="q" class="form-control border-0" placeholder="pesquisar rápida..." autocomplete="off">
					</form>
				</div>

				<div class="categoria-blog">
					<ul id="lista-categorias"></ul>
				</div>

				<div class="my-3">
					<!-- <img src="http://dummyimage.com/800x600/4d494d/686a82.gif&text=placeholder+image" class="img-fluid" alt="placeholder+image"> -->
				</div>


			</div>

			</div>
		</div>

		<div class="clearfix"></div>
	</main>



