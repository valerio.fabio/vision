<?php

$slider = new db();
$slider->query( "SELECT *
 FROM     banner
 ORDER BY ban_order ASC" );
//$slider->execute();

if( !empty($slider->execute() )){

  ?>

  <div id="carousel-slider" class="carousel slide wow bounceInUp" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php for($i=0; $i<$slider->num_linha; $i++){ ?>
        <li data-target="#carousel-slider" data-slide-to="<?php echo $i; ?>" <?php if($i == '0' ){ echo 'class="active"'; }?>></li>
      <?php } ?>
    </ol>

    <div class="carousel-inner" role="listbox">

      <?php
      $aux = 0;
      foreach( $slider->row() AS $row ){
        ?>
        <div class="carousel-item <?php if($aux == '0' ){ echo 'active'; }?>"  role="listbox"  style="background-image: url('painel/images/fotos-banner/<?php echo $row['ban_id']; ?>/g/<?php echo $row['ban_capa']; ?>'); background-position:<?php echo $row['ban_posicao']; ?>!important;">

          <?php if( !empty($row['ban_link']) ){?>
            <a href="<?php echo $row['ban_link']; ?>" title="<?php echo $row['ban_titulo']; ?>">
            <?php } ?>
            <div class="container">
            <div class="row container-slide">
              <div class="col-lg-8 align-self-center text-slider">
                <?php if( !empty($row['ban_titulo']) ){?><h2><?php echo $row['ban_titulo']; ?></h2><?php }?>
                <?php if( !empty($row['ban_descricao']) ){?><p><?php echo $row['ban_descricao']; ?></p><?php }?>
              </div>
              <div class="col-lg-4 align-self-center text-slider">
                <?php if( !empty($row['ban_include']) ) include( 'pages/'.$row['ban_include'] );?>
              </div>
            </div>
            </div>

            <?php if( !empty($row['ban_link']) ){?>
            </a>
          <?php } ?>

        </div>
        <?php
        $aux++;
      }
      ?>

      <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>

  </div>

  <?php } ?>