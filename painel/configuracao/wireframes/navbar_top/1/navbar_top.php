<div class="bar p-3 wow bounceInDown aos-item" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-xl-8 col-lg-7 col-md-6 d-md-flex">
					<div class="flex-fill btn-nav-top d-none d-md-block">
                        <a href="#" target="new" class="btn btn-success btn-sm"><i class="fas fa-whatsapp fa-fx"></i> <span>12 99999-9999</span></a>
					</div>
			</div>

			<div class="col-xl-4 col-lg-5 col-md-6 text-md-right">
				<a href="fale-conosco" class="btn btn-danger btn-sm"> Fale Conosco </a>

				<span class="mx-3">|</span>

				[[midias-sociais]]
			</div>
		</div>
	</div>
</div>