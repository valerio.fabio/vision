<div class="container">
	<div class="row my-4">

		<div class="col-12">
			<h1 id="pag_titulo"></h1>
			<!--<div class="lead" id="pag_mini_texto"></div>-->
		</div>

		<div class="col-12 my-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo $url ?>"><i class="fas fa-home"></i></a></li>
					<li class="breadcrumb-item"><a href="#" id="breadcrumb-page"></a></li>
					<li class="breadcrumb-item active" aria-current="page" id="breadcrumb-titulo">Voltar</li>
				</ol>
			</nav>
		</div>

		<div class="col-12 mt-3" id="capa-imagem"></div>
		<div class="col-12  d-flex align-items-center">
			<p class="mt-3" id="menu_descricao"></p>
		</div>
	</div>
</div>

<main id="pagina" class="mb-4">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div id="pad_texto"></div>
				<div class="clearfix"></div>
			</div>
			<div id="comentario-facebook"></div>
		</div>
	</div>
</main>