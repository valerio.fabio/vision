<?php

include('assets/php/jsonConf.php');

?>



<section class="py-4 bg-light" data-aos="fade-down">

	<div class="container">

		<div class="row">

			<div class="col-12">

				[[texto-2]]

				<form class="news" method="post">

					<label class="sr-only" for="inputNews">Seu e-mail</label>

					<div class="input-group mb-2">

						<input type="mail" name="email" class="form-control form-control-lg" id="inputNews" placeholder="Seu e-mail" required>

						<div class="input-group-prepend">

							<div class="input-group-text badge-success">

								<input type="hidden" value="newsletter" name="pagina">

								<button type="submit" class="btn p-0">

									<i class="fas fa-check text-white"></i>

								</button>

							</div>

						</div>

					</div>

					<small>*Fique tranquilo, nós também não gostamos de SPAM.</small>

				</form>

			</div>

		</div>

	</div>

</section>



<footer class="">

	<div class="<?php echo $jsonConf->c_footer_largura?>">

		<div class="row">

			<div class="col-xl-2 col-12 align-self-center text-center">

				<img src="<?php echo $url ?>painel/logo/footer/logo.png" class="logo-footer"	alt="<?php echo $jsonConf->c_nome ?>">

			</div>

			<div class="col-xl-3 col-lg-3 col-md-6">

				<ul class="list-group list-group-flush">

					<li class="list-group-item disabled"><h5 class="lead">Entre em contato</h5></li>

					<li class="list-group-item">(12) 3625-5433</li>

					<li class="list-group-item">atendimento@diconcontabilidade.com.br</li>

				</ul>

			</div>

			<div class="col-xl-2 col-lg-3 col-md-6">

				<ul class="list-group list-group-flush">

					<li class="list-group-item disabled"><h5 class="lead">Acessos</h5></li>

					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>contato">Fale Conosco</a></li>

					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>trabalhe-conosco">Trabalhe Conosco</a></li>

					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>links-uteis">Link Úteis</a></li>

					<li class="list-group-item"><a class="text-white" href="#">Agendar uma reunião</a></li>

					<li class="list-group-item"><a class="text-white" href="#">Solicitar Orçamento</a></li>

					<li class="list-group-item"><a class="text-white" href="#">Abertura de Empresa</a></li>

				</ul>

			</div>

			<div class="col-xl-2 col-lg-3 col-md-6">

				<ul class="list-group list-group-flush">

					<li class="list-group-item disabled"><h5 class="lead">Acessos</h5></li>

					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>contato">Fale Conosco</a></li>

					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>trabalhe-conosco">Trabalhe Conosco</a></li>

					<li class="list-group-item"><a class="text-white" href="<?php echo $url ?>links-uteis">Link Úteis</a></li>

					<li class="list-group-item"><a class="text-white" href="#">Agendar uma reunião</a></li>

					<li class="list-group-item"><a class="text-white" href="#">Solicitar Orçamento</a></li>

					<li class="list-group-item"><a class="text-white" href="#">Abertura de Empresa</a></li>

				</ul>

			</div>

		</div>



	</div>

</footer>