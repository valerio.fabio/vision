<?php
include('assets/php/jsonConf.php');
?>
<footer class="">
	<div class="<?php echo $jsonConf->c_footer_largura?>">

		<div class="row mt-5">

			<div class="col-lg-12 text-center mb-3">
                <img src="<?php echo $url ?>painel/logo//footer/logo.png" alt="">
			</div>

			<div class="col-lg-12 mt-3">
				<ul class="nav-footer text-center">
				[[midias-sociais]]
				</ul>
			</div>

		</div>
	</div>
</footer>
