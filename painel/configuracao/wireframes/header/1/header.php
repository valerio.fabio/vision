<header class="aos-item" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
	<?php  include('_home.php'); ?>

	<!-- Páginas dinâmicas -->
	
	
	<section class="home-card-box">
		<div class="container">
			<div class="row">
				<div id="pagina-home"></div>
			</div>
		</div>
	</section>

	<?php
	StrReplace('painel/configuracao/wireframes/home/'.$home.'/home.php');
	?>

</header>