<?php
ob_start();
session_start();

include('../php/db.class.php');
include('../php/function.php');
include('../php/htaccess.php');

/*Validar Usuurio*/
if( empty( $_COOKIE['user_id'] ) )
{
 echo '<meta http-equiv="refresh" content="0;URL='.$url.'login.php">';
 exit;
}
?>

<link rel="stylesheet" href="<?php echo $url; ?>assets/editor-less/codemirror.css">
<link rel="stylesheet" href="<?php echo $url; ?>assets/editor-less/themes/icecoder.css">

<script src="<?php echo $url; ?>assets/editor-less/codemirror.js"></script>

<script src="<?php echo $url; ?>assets/editor-less/codemirror.js"></script>
<script src="<?php echo $url; ?>assets/editor-less/javascript.js"></script>
<script src="<?php echo $url; ?>assets/editor-less/active-line.js"></script>
<script src="<?php echo $url; ?>assets/editor-less/matchbrackets.js"></script>
<script src="<?php echo $url; ?>assets/editor-less/css.js"></script>
<style>.CodeMirror {border: 1px solid #ddd; line-height: 1.2;}</style>

<?php
/*configuração*/

include('../php/__construct/_wireframes_edit.php');


$__configSql = "SELECT * FROM configuracao WHERE tipo = 'conf'";
$__config = new db();     
$__config->query($__configSql);
$__config->execute();
$result__config = $__config->object();

$result__config->estruturacao;

$edi = json_decode($result__config->estruturacao);

// print_r($result__config->estruturacao);

// echo $jsonConf->c_tamanho_image;
//

/*  \configuracao */

?>

<a class="btn btn-warning" href="<?php echo $url?>!/<?php echo $link[1]?>/visualizar">Voltar</a>
<hr>
<h2 class="head">Editar Configura&ccedil;&atilde;o</h2>

<form enctype="multipart/form-data" action="<?php echo $url.'!/'.$link[1].'/'.$link[2]?>/envio" id="CadConfig" method="post" class="row mb-5">

  <div class="col-md-1 py-2">
    <label>PX de Imagem</label>
    <br>
    <input class="form-control" name="c_tamanho_image" type="text" id="c_tamanho_image" autocomplete="off" value="<?php echo $edi->c_tamanho_image?>" maxlength="200" />
  </div>

  <div class="col-md-3 py-2">
    <label>URL Desenvolvedor</label>
    <br>
    <input class="form-control" name="c_url" type="text" id="c_url" autocomplete="off" value="<?php echo $edi->c_url?>" maxlength="200" />
  </div>

  <div class="col-md-3 py-2">
    <label>URL Site</label>
    <br>
    <input class="form-control" name="c_site" type="text" id="c_site" autocomplete="off" value="<?php echo $edi->c_site?>" maxlength="200" />
  </div>

  <div class="col-md-2 py-2">
    <label>Banner Home</label>
    <br>
    <input class="form-control" name="c_tamanho_banner" type="text" id="c_tamanho_banner" autocomplete="off" value="<?php echo $edi->c_tamanho_banner?>" maxlength="200" />
  </div>

  <div class="col-md-3 py-2">
    <label>TITLE - Head</label>
    <br>
    <input class="form-control" name="c_nome" type="text" id="c_nome" autocomplete="off" value="<?php echo $edi->c_nome?>" maxlength="50" max="50" />
  </div>

  <div class="col-md-5 py-2">
    <label>Título Home</label>
    <br>
    <input class="form-control" name="c_titulo_home" type="text" id="c_titulo_home" autocomplete="off" value="<?php echo $edi->c_titulo_home?>" maxlength="50" max="50" />
  </div>

  <div class="col-md-7 py-2">
    <label>Título Formul&aacute;rio (Wiriframe 8)</label>
    <br>
    <input class="form-control" name="c_titulo_orcamento" type="text" id="c_titulo_orcamento" autocomplete="off" value="<?php echo $edi->c_titulo_orcamento?>" maxlength="100" max="100" />
  </div>

  <div class="col-md-12 py-2">
    <h3>Script</h3>
  </div>
<!-- 
  <div class="col-md-6 py-2">
    <label>Chave do Google Analytics</label>
    <br>
    <input class="form-control" name="c_google_analytics" type="text" id="c_google_analytics" autocomplete="off" value="<?php echo $edi->c_google_analytics?>" maxlength="50" max="50" />
  </div>

  <div class="col-md-6 py-2">
    <label>Chave do Google Webmaster</label>
    <br>
    <input class="form-control" name="c_google_webmaster" type="text" id="c_google_webmaster" autocomplete="off" value="<?php echo $edi->c_google_webmaster?>" maxlength="50" max="50" />
  </div>

  <div class="col-md-6 py-2">
    <label>Chave do Google Tags</label>
    <br>
    <input class="form-control" name="c_google_tags" type="text" id="c_google_tags" autocomplete="off" value="<?php echo $edi->c_google_tags?>" maxlength="50" max="50" />
  </div>

  <div class="col-md-6 py-2">
    <label>Chave do Google Otmize</label>
    <br>
    <input class="form-control" name="c_google_otimize" type="text" id="c_google_otimize" autocomplete="off" value="<?php echo $edi->c_google_otimize?>" maxlength="50" max="50" />
  </div> -->

  <div class="col-12 py-2">
    <!-- <label>SCRIPTS</label> -->
    <textarea id="c_scripts_list" class="form-control" style="font-size: 11px; height: 300px"><?php echo $edi->c_scripts?></textarea>
  </div>

  <div class="col-md-12 py-2">
    <h3>&Iacute;CONES M&Iacute;DIAS</h3>
  </div>

  <div class="col-md-6 py-2">
    <label>Style Icon Mídias (top) <code>color|outline|circulo|quadrado|size-midia</code></label>
    <br>
    <div class="d-flex justify-content-between align-items-center">
    <div class="form-group form-check"><label for="c_style_midias_top_color"><input type="checkbox" id="c_style_midias_top_color" class="form-check-input" name="c_style_midias_top_color" <?php if( $edi->c_style_midias_top_color == 'true' ) echo 'checked' ?> value="true">Color</label></div>
    <div class="form-group form-check"><label for="c_style_midias_top_borda"><input type="checkbox" id="c_style_midias_top_borda" class="form-check-input" name="c_style_midias_top_borda" <?php if( $edi->c_style_midias_top_borda == 'true' ) echo 'checked' ?> value="true">Somente Borda</label></div>
    <div class="form-group form-check"><label for="c_style_midias_top_circulo"><input type="checkbox" id="c_style_midias_top_circulo" class="form-check-input" name="c_style_midias_top_circulo" <?php if( $edi->c_style_midias_top_circulo == 'true' ) echo 'checked' ?> value="true">Circulo</label></div>
    <div class="form-group form-check"><label for="c_style_midias_top_tamanho"><input type="number" id="c_style_midias_top_tamanho" class="form-control" name="c_style_midias_top_tamanho" value="<?php echo $edi->c_style_midias_top_tamanho?>"></label></div>
    </div>

  </div>

  <div class="col-md-6 py-2">
    <label>Style Icon Mídias (footer) <code>color|outline|circulo|quadrado|size-midia</code></label>
    <br>
    <div class="d-flex justify-content-between align-items-center">
    <div class="form-group form-check"><label for="c_style_midias_footer_color"><input type="checkbox" id="c_style_midias_footer_color" class="form-check-input" name="c_style_midias_footer_color" <?php if( $edi->c_style_midias_footer_color == 'true' ) echo 'checked' ?> value="true">Color</label></div>
    <div class="form-group form-check"><label for="c_style_midias_footer_borda"><input type="checkbox" id="c_style_midias_footer_borda" class="form-check-input" name="c_style_midias_footer_borda" <?php if( $edi->c_style_midias_footer_borda == 'true' ) echo 'checked' ?> value="true">Somente Borda</label></div>
    <div class="form-group form-check"><label for="c_style_midias_footer_circulo"><input type="checkbox" id="c_style_midias_footer_circulo" class="form-check-input" name="c_style_midias_footer_circulo" <?php if( $edi->c_style_midias_footer_circulo == 'true' ) echo 'checked' ?> value="true">Circulo</label></div>
    <div class="form-group form-check"><label for="c_style_midias_footer_tamanho"><input type="number" id="c_style_midias_footer_tamanho" class="form-control" name="c_style_midias_footer_tamanho" value="<?php echo $edi->c_style_midias_footer_tamanho?>"></label></div>
    </div>

  </div>

  <div class="col-md-12 py-2 form-group wireframes">
    <h2>Wireframes* Home</h2>
    <hr>
  <div class="container-fluid">
    <div class="row" role="group" aria-label="...">
      <?php
      $pasta = '../configuracao/wireframes/home/';
      $__wire = new WireFrames();
      $__wire->wire('c_home', $edi->c_home, $pasta, $url);
     ?>
     </div>
     <div class="clearfix"></div>
   </div>
 </div>

 <hr>

 <div class="col-md-12 py-2 form-group wireframes">
  <h2>WIREFRAMES* NAVBAR</h2>
  <hr>
  <div class="container-fluid">
    <div class="row" role="group" aria-label="...">
      <?php
        $pasta = '../configuracao/wireframes/navbar/';
        $__wire = new WireFrames();
        $__wire->wire('c_nav', $edi->c_nav, $pasta, $url);
     ?>
   </div>
   <div class="clearfix"></div>

 </div>
</div>

<div class="col-md-3 py-2">
  <label>Organizar menu</label>
  <br>
  <select name="c_menu_direcao" id="c_menu_direcao" class="custom-select">
    <option value="mr-auto" <?php if( $edi->c_menu_direcao == 'mr-auto' ) echo 'selected' ?>>Esquerda</option>
    <option value="ml-auto" <?php if( $edi->c_menu_direcao == 'ml-auto' ) echo 'selected' ?>>Direita</option>
    <option value="mx-auto" <?php if( $edi->c_menu_direcao == 'mx-auto' ) echo 'selected' ?>>Centro</option>
  </select>
</div>

<div class="col-md-3 py-2">
  <label>Menu Largura</label>
  <br>
  <select name="c_menu_largura" id="c_menu_largura" class="custom-select">
    <option value="container" <?php if( $edi->c_menu_largura == 'container' ) echo 'selected' ?>>Container</option>
    <option value="container-fluid" <?php if( $edi->c_menu_largura == 'container-fluid' ) echo 'selected' ?>>Container-Fluid</option>
  </select>
</div>

<div class="col-md-3 py-2">
  <label>Categoria</label>
  <br>
  <?php echo SelectON( 'c_categoria', $edi->c_categoria ); ?>
</div>

<div class="col-md-3 py-2">
  <label>Sub-Categoria</label>
  <br>
  <?php echo SelectON( 'c_subcategoria', $edi->c_subcategoria ); ?> 
</div>

<div class="col-md-3 py-2">
  <label>Menu</label>
  <br>
  <?php echo SelectON( 'c_menu', $edi->c_menu ); ?>
</div>

<div class="col-md-3 py-2">
  <label>Busca no NaV (Dependendo do tema)</label>
  <br>
  <?php echo SelectON( 'c_busca', $edi->c_busca ); ?>
</div>


<div class="col-md-3 py-2">
  <label>Barra Topo Site</label>
  <br>
  <?php echo SelectON( 'c_bar', $edi->c_bar ); ?>
</div>

<hr>

<div class="col-md-12 py-2 form-group wireframes">
  <h2>WIEREFRAMES* HEADER</h2>
  <hr>
  <div class="container-fluid">
    <div class="row" role="group" aria-label="...">
      <?php
          $pasta = '../configuracao/wireframes/header/';
          $__wire = new WireFrames();
          $__wire->wire('c_header', $edi->c_header, $pasta, $url);
     ?>
   </div>
   <div class="clearfix"></div>
 </div>
</div>

<hr>

<div class="col-md-12 py-2 form-group wireframes">
  <h2>WIREFRAMES* SLIDER</h2>
  <hr>
  <div class="container-fluid">
    <div class="row" role="group" aria-label="...">
      <?php
        $pasta = '../configuracao/wireframes/slider/';
        $__wire = new WireFrames();
        $__wire->wire('c_slider', $edi->c_slider, $pasta, $url);
     ?>
   </div>
   <div class="clearfix"></div>
 </div>
</div>

<hr>

<div class="col-md-12 py-2 form-group wireframes">
  <h2>WIREFRAMES* FOOTER</h2>
  <hr>
  <div class="container-fluid">
    <div class="row" role="group" aria-label="...">
      <?php
            $pasta = '../configuracao/wireframes/footer/';
            $__wire = new WireFrames();
            $__wire->wire('c_footer', $edi->c_footer, $pasta, $url);
     ?>
   </div>
   <div class="clearfix"></div>
 </div>
</div>

<div class="col-md-3 py-2">
  <label>Footer Largura</label>
  <br>
  <select name="c_footer_largura" id="c_footer_largura" class="custom-select">
    <option value="container" <?php if( $edi->c_footer_largura == 'container' ) echo 'selected' ?>>Container</option>
    <option value="container-fluid" <?php if( $edi->c_footer_largura == 'container-fluid' ) echo 'selected' ?>>Container-Fluid</option>
  </select>
</div>

<hr>

<div class="col-md-12 py-2">
  <h3>CONFIGURAÇÕES</h3>
</div>

<div class="col-md-3 py-2">
  <label>SEO</label>
  <br>
  <?php echo SelectON( 'c_seo', $edi->c_seo ); ?>
</div>

<div class="col-md-3 py-2">
  <label>Bloco de Texto TEXTEARE</label>
  <br>
  <?php echo SelectON( 'c_texto', $edi->c_texto ); ?>
</div>

<div class="col-md-3 py-2">
  <label>Configura&ccedil;&atilde;o do Send Email</label>
  <br>
  <?php echo SelectON( 'c_email', $edi->c_email ); ?>
</div>

<div class="col-md-3 py-2">
  <label>Mapa</label>
  <br>
  <?php echo SelectON( 'c_mapa', $edi->c_mapa ); ?> 
</div>

<div class="col-md-3 py-2">
  <label>Mapa + Dados</label>
  <br>
  <?php echo SelectON( 'c_mapa_dados', $edi->c_mapa_dados ); ?> 
</div>

<div class="col-md-3 py-2">
  <label>Menu Rodap&eacute;</label>
  <br>
  <?php echo SelectON( 'c_menu_rodape', $edi->c_menu_rodape ); ?>
</div>

<div class="col-md-3 py-2">
  <label>QrCode</label>
  <br>
  <?php echo SelectON( 'c_qrcode', $edi->c_qrcode ); ?>
</div>

<div class="col-md-3 py-2">
  <label>Anexo</label>
  <br>
  <?php echo SelectON( 'c_anexo', $edi->c_anexo ); ?>
</div>

<div class="col-md-3 py-2">
  <label>NewsLetter no Site?</label>
  <br>
  <?php echo SelectON( 'c_news', $edi->c_news ); ?> 
</div>

<div class="col-md-3 py-2">
  <label>Breadcrumb nas P&aacute;ginas</label>
  <br>
  <?php echo SelectON( 'c_breadcrumb', $edi->c_breadcrumb ); ?>
</div>

<div class="col-md-3 py-2">
  <label>RSS</label>
  <br>
  <?php echo SelectON( 'c_rss', $edi->c_rss ); ?>
</div>

<div class="col-md-3 py-2">
  <label>CSS</label>
  <br>
  <?php echo SelectON( 'c_css', $edi->c_css ); ?>
</div>

<div class="col-md-3 py-2">
  <label>   <i class="fab fa-dev"></i>  Desenvolvedor</label>
 <br>
 <?php echo SelectON( 'c_dev', $edi->c_dev ); ?>
</div>

<div class="col-md-3 py-2">
  <label>Landpage</label>
  <br>
  <?php echo SelectON( 'c_land', $edi->c_land ); ?>
</div>

<div class="col-md-6 py-2">
  <label>Efeito Slider <code>(Ativo = Fade / Inativo = Deslisar)</code></label>
  <br>
  <?php echo SelectON( 'c_efeito_slider', $edi->c_efeito_slider ); ?>
</div>


<div class="clearfix"></div>
<hr>
<div class="clearfix"></div>
<div class="col-md-12 py-2">
  <input type="button" value="Salvar" id="salvarConf"  class="btn btn-danger form-control" /> 
</div>

</form>

<div id="mascaraConf"> -- </div>


<script type="text/javascript" language="javascript">


  /* REGISTRO */

  $(document).ready(function() {


    var editor = CodeMirror.fromTextArea(document.getElementById("c_scripts_list"), {
        lineNumbers: true,
        styleActiveLine: true,
        matchBrackets: true,
        mode: "javascript",
        matchBrackets: true,
        lineWiseCopyCut: true
      });


    $('#salvarConf').click(function() {

      console.log('>>>>'+$('#c_scripts').val());

      var dados = $('#CadConfig').serialize();
      var registro = '<?php echo $url; ?>configuracao/registro.php';
      var editorCode = editor.getValue();


      console.log(editorCode);

      $.ajax({
        url : registro,
        type : 'post',
        data : dados+"&c_scripts="+editorCode,
        beforeSend : function(){
          console.log(dados);
         $("#mascaraConf").html("ENVIANDO...");
         
       }
     })
      .done(function(msg){
        $("#mascaraConf").html(msg);
      })
      .fail(function(jqXHR, textStatus, msg){
        $("#mascaraConf").html(msg);
      }); 

    });
  });
</script>