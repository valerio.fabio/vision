<script>
  var $ativeMenu = jQuery.noConflict(); 
  $ativeMenu(document).ready(function(){
   $ativeMenu( "#products" ).addClass( "show" );
 });
</script>

<?php

$bannerSQL = "SELECT * FROM banner ORDER BY ban_titulo ASC";

$banner = new db();     
$banner->query($bannerSQL);
$banner->execute();

?>

<a class="btn btn-outline-success" href="<?php echo $url.'!/'.$link[1]?>/cadastro">Novo cadastro</a>
        <a class="btn btn-outline-info" href="<?php echo $url?>!/<?php echo $link[1]; ?>/ordenar"><i class="fas fa-sort"></i> Ordenar</a>

<hr>

<h2 class="display-4 mb-3">Banners  &bull; <span>Lista (<?php echo $banner->rowCount()?>)</span></h2>

<h5 class="badge-warning p-3">Tamanho: <?php echo $jsonConf->c_tamanho_banner; ?> pixels - m&aacute;ximo 500KB</h5>



<div class="card">
      <div class="card-body">
        <table class="table table-striped table-hover" id="dataTables-example">
          <thead>
            <tr>
              <th width="50">ID</th>
              <th>T&iacute;tulo</th>
              <th>Posição</th>
              <th>Order</th>
              <th>Capa</th>
              <th>Conteúdo</th>
              <th align="right" width="150"></th>
            </tr>
          </thead>
          <tbody>
           <?php
            if( !empty($banner->row()) ){
                foreach( $banner->row() AS $row ){
              ?>
              <tr class="odd gradeX">
                <td><?php echo $row['ban_id']?></td>
                <td><?php echo $row['ban_titulo']?></td>
                <td><?php echo $row['ban_posicao']?></td>
                <td><?php echo $row['ban_order']?></td>
                <td width="100" class="text-center">
                  <?php echo !empty($row['ban_capa']) ? '<i class="fas fa-check-circle text-success fa-2x"></i>' : '<i class="fas fa-times-circle text-danger fa-2x"></i>'; ?>
                </td>
                <td width="100" class="text-center">
                  <?php echo !empty($row['ban_conteudo']) ? '<i class="fas fa-check-circle text-success fa-2x"></i>' : '<i class="fas fa-times-circle text-danger fa-2x"></i>'; ?>
                </td>
                <td align="right" >
                  <a href="<?php echo $url?>!/<?php echo $link[1]?>/fotos/<?php echo $row['ban_id']?>" class="btn btn-sm btn-outline-warning">
                    <i class="far fa-images"></i>
                  </a>
                  <a href="<?php echo $url?>!/<?php echo $link[1]?>/editar/<?php echo $row['ban_id']?>" class="btn btn-sm btn-outline-success">
                    <i class="fas fa-edit"></i>
                  </a>
                  <a href="<?php echo $url?>!/<?php echo $link[1]?>/deletar/<?php echo $row['ban_id']?>/&titulo=<?php echo $row['ban_titulo']; ?>" class="btn btn-sm btn-outline-danger">
                    <i class="far fa-trash-alt"></i>
                  </a>
                </td>
              </tr>
              <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>
</div>


<!-- jQuery -->
<script src="<?php echo $url?>bower_components/jquery/dist/jquery.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo $url?>bower_components/metisMenu/dist/metisMenu.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo $url?>bower_components/datatables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo $url?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>


<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
  var $tabelas = jQuery.noConflict(); 
  $tabelas(document).ready(function() {
    $tabelas('#dataTables-example').DataTable({
      responsive: true
    });
  });
</script>

