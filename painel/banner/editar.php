<script>
  var $ativeMenu = jQuery.noConflict(); 
  $ativeMenu(document).ready(function(){
   $ativeMenu( "#products" ).addClass( "show" );
 });
</script>

<?php

$edi = new db();
$edi->query( "SELECT * FROM banner WHERE ban_id = '".$link[3]."'" );
$edi->execute();
$row = $edi->object();


?>

<a class="btn btn-outline-warning" href="<?php echo $url?>!/<?php echo $link[1]?>/visualizar">Voltar</a>

<hr>

<h2 class="head">Editar Banner &bull; <span>Cadastro</span></h2>

<form enctype="multipart/form-data" id="form" method="post">
  <table class="table">
    <tr>
      <th width="150" valign="middle">T&iacute;tulo</th>
      <td valign="middle" colspan="4">
        <input name="ban_titulo" class="form-control" type="text" autofocus id="ban_titulo" autocomplete="off" value="<?php echo $row->ban_titulo?>" maxlength="50" /> 
        <small>*50 caract&eacute;res</small>
      </td>
    </tr>
    <tr>
      <th width="150" valign="middle">Texto</th>
      <td valign="middle" colspan="4"><input class="form-control" name="ban_descricao" type="text" id="ban_descricao" maxlength="100" value="<?php echo $row->ban_descricao?>" />
       <small>*100 caract&eacute;res</small>
     </td>
   </tr>
   <tr>
    <th>Link</th>
    <td colspan="4"><input name="ban_link" type="text" class="form-control" id="ban_link" autocomplete="off" value="<?php echo $row->ban_link?>" /></td>
  </tr>
     <tr>
      <th>Exibir texto no banner?</th>
      <td>
        <select name="ban_conteudo" class="form-control">
           <option value="1" <?php if($row->ban_conteudo == '1') echo 'selected';?>>Sim</option>
           <option value="0" <?php if($row->ban_conteudo == '0') echo 'selected';?>>Não</option>
        </select>
      </td>
      <th width="150">Posição?</th>
      <td>
        <select name="ban_posicao" class="form-control">
           <option <?php if( $row->ban_posicao == 'center center' ) echo 'selected' ?>value="center center" selected>center center</option>
           <option <?php if( $row->ban_posicao == 'center left' ) echo 'selected' ?>value="center left">center left</option>
           <option <?php if( $row->ban_posicao == 'center right' ) echo 'selected' ?>value="center right">center right</option>
           <option <?php if( $row->ban_posicao == 'center top' ) echo 'selected' ?>value="center top">center top</option>
           <option <?php if( $row->ban_posicao == 'center bottom' ) echo 'selected' ?>value="center bottom">center bottom</option>
           <option <?php if( $row->ban_posicao == 'top left' ) echo 'selected' ?>value="top left">top left</option>
           <option <?php if( $row->ban_posicao == 'top right' ) echo 'selected' ?>value="top right">top right</option>
           <option <?php if( $row->ban_posicao == 'bottom left' ) echo 'selected' ?>value="bottom left">bottom left</option>
           <option <?php if( $row->ban_posicao == 'bottom right' ) echo 'selected' ?>value="bottom right">bottom right</option>
        </select>
      </td>
    </tr>


   <tr>
    <td colspan="4">
      <label>
      <i class="fa fa-info-circle text-sm box-ico bg-danger" data-toggle="tooltip" data-placement="right" title="Cuidado ao manipular a inclus&atilde;o de p&aacute;ginas externa" ></i>
      Include (Incluir p&aacute;ginas externas) 
    </label>

    <?php
    $pages_includes = glob("../pages/{*.php}", GLOB_BRACE);
    ?>
    <select name="ban_include" class="form-control">
      <option value="0">Nenhum</option>
      <?php
      foreach( $pages_includes as $valPages ){
        $valPages = @end( @explode('/', $valPages) );
        ?>
        <option value="<?php echo $valPages?>" <?php if( $row->ban_include == $valPages ){ echo 'selected'; } ?>><?php echo $valPages?></option>
        <?php
      }
      ?>
    </select>
  </td>
  </tr>
  <tr>
    <td colspan="4">
      <input type="button" name="Enviar" id="salvar" value="Enviar" class="btn btn-success w-100" /> 
      <input type="hidden" name="redirecionar" value="visualizar">  <!--Redirecionar-->
      <input type="hidden" name="tabela" value="banner">            <!--Tabela-->
      <input type="hidden" name="url" value="<?php echo $url ?>">      <!--URL-->
      <input type="hidden" name="ban_id" value="<?php echo $row->ban_id?>">                <!--Valor Editar-->
    </td>
  </tr>
</table>
</form>

<div id="result"></div>

<script src="<?php echo $url ?>php/db.class.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" language="javascript">
  /* EDITAR */
  editar('<?php echo $url; ?>', 'ban_id', '<?php echo $row->ban_id ?> ', '');
</script>