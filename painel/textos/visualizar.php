<link rel="stylesheet" type="text/css" href="<?php echo $url; ?>assets/datatables/jquery.dataTables.min.css"></style>
<script src="<?php echo $url; ?>assets/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function() {
    jQuery('#dataTables').DataTable( {
      stateSave: true,
      responsive: true,
      "order": [[ 1, 'asc' ]],
      "pagingType": "full_numbers"
    } );

    jQuery('.dataTables_length select').addClass('input-control');
    jQuery('.dataTables_filter label input').addClass('input-control');
    jQuery('.table').addClass('font-12');
  } );
</script>
<?php
$InputSQL = new db();
$InputSQL->query( "SELECT * FROM textos ORDER BY tex_id ASC" );
$InputSQL->execute();

if(  $jsonConf->c_texto == '1' ){
  ?>
  <a class="btn btn-outline-success" href="<?php echo $url?>!/<?php echo $link[1]?>/cadastro">Novo cadastro</a>
  <hr>
  <?php 
}else{    
  $block = 'disabled';
}
?>

<h2 class="display-4 mb-3">P&aacute;ginas com Textos <small>(<?php echo $InputSQL->rowCount()?>)</small></h2>

<div class="card">
  <div class="card-body">
    <table class="table table-striped table-hover" id="dataTables-example">
      <thead>
        <tr>
          <th width="20">ID</th>
          <th>T&iacute;tulo</th>
          <th>Implanta&ccedil;&atilde;o</th>
          <th width="140" align="right"></th>
        </tr>
      </thead>
      <tbody>
       <?php
       if( !empty($InputSQL->row()) ){
        foreach( $InputSQL->row() AS $row ){
          ?>
          <tr class="odd gradeX">
            <td><?php echo $row['tex_id']?></td>
            <td><?php echo $row['tex_titulo']?></td>
            <td>[[texto-<?php echo $row['tex_id']?>]]</td>
            <td width="150" class="text-right">
              <a href="<?php echo $url?>!/<?php echo $link[1]?>/editar/<?php echo $row['tex_id']?>" class="btn btn-outline-success btn-sm"><i class="fas fa-edit"></i></a>
              <a  href="<?php echo $url?>!/<?php echo $link[1]?>/deletar/<?php echo $row['tex_id']?>" class="btn btn-outline-danger btn-sm <?php echo $block?> <?php if( $row['tex_id'] <= 4 ) echo 'disabled' ?>"><i class="far fa-trash-alt"></i></a>
            </td>
          </tr>
          <?php
        }
      }
      ?>
    </tbody>
  </table>
</div>
</div>

