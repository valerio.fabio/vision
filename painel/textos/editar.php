
<?php
$edi = new db();
$edi->query( "SELECT * FROM textos WHERE tex_id = '".$link[3]."'" );
$edi->execute();
$row = $edi->object();

include('assets/tinymce-4.6.5/index.php'); 
?>

<style>
.mce-notification{
  display: none !important;
}
</style>

<a class="btn btn-outline-warning" href="<?php echo $url?>!/<?php echo $link[1]?>/visualizar">Voltar</a>
<hr>
<h2 class="display-4 mb-3">Editar &bull; Texto</h2>

<div class="card">
  <div class="card-body">
    <form enctype="multipart/form-data" id="form" method="post">
      <table class="table table-striped">
        <tr>
          <td>
            <p>T&iacute;tulo</p>
            <input class="form-control" name="tex_titulo" type="text" autofocus id="tex_titulo" autocomplete="off" value="<?php echo $row->tex_titulo; ?>" size="60" maxlength="200" /> 
          </td>
          <td>
            <p>Exibir Título</p>
            <select name="text_exibi_titulo" id="text_exibi_titulo" class="form-control">
              <option value="1" <?php if( $row->text_exibi_titulo == '1' ){ echo 'selected'; }?>>Ativo</option>
              <option value="0" <?php if( $row->text_exibi_titulo == '0' ){ echo 'selected'; }?>>Inativo</option>
            </select>
          </td>
        </tr>
        <tr>
          <!-- HELP -->
          <?php include('includes/#help.php'); ?>
          <td colspan="5"><textarea class="form-control" name="tex_texto" id="tex_texto"><?php echo $row->tex_texto; ?></textarea></td>
        </tr>
        <tr>
          <td colspan="5">
            <input  type="button" name="Enviar" id="salvar" value="Editar" class="btn btn-success w-100" />
            <input type="hidden" name="redirecionar" value="visualizar">  <!--Redirecionar-->
            <input type="hidden" name="tabela" value="textos">           <!--Tabela-->
            <input type="hidden" name="url" value="<?php echo $url ?>">      <!--URL-->
            <input type="hidden" name="tex_id" value="<?php echo $link[3]?>">                <!--Valor Editar-->
          </td>
        </tr>
      </table>
    </form>
  </div>
</div>

<div id="result"></div>

<script src="<?php echo $url ?>php/db.class.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" language="javascript">
  /* EDITAR */
  editar('<?php echo $url; ?>', 'tex_id', '<?php echo $link[3]?> ', 'tinyMCE');
</script>