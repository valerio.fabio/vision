<?php



//Categorias Produtos
$InputSQL = new db();
$InputSQL->query( "SELECT * FROM midias ORDER BY midia_id ASC" );
$InputSQL->execute();

?>

<a class="btn btn-outline-success" href="<?php echo $url?>!/<?php echo $link[1]?>/cadastro">Novo cadastro</a>

<hr>

<h2>M&iacute;dias Sociais<small> (<?php echo $InputSQL->num_linha?>)</small></h2>


    <div id="wrapper">

        <!-- Navigation -->
        

        <div id="page-wrapper2">
            <div class="row">
                <div class="col-lg-12">

                    <table class="table id="dataTables-example">
                      <thead>
                        <tr>
                          <th width="20">ID</th>
                          <th width="20">Midia</th>
                          <th>Link</th>
                          <th width="100" align="right"></th>
                        </tr>
                      </thead>
                      <tbody>
                      
                       <?php
                        foreach( $InputSQL->row() AS $row )
                        {
                        ?>
                        
                        <tr class="odd gradeX">
                          <td><?php echo $row['midia_id']?></td>
                          <td class="text-center"><i class="<?php echo $row['midia_tipo']?>"></i></td>
                          <td><?php echo $row['midia_link']?></td>
                          
                          <td align="right" >
                              <a href="<?php echo $url?>!/<?php echo $link[1]?>/editar/<?php echo $row['midia_id']?>" class="btn btn-sm btn-outline-success"><i class="fas fa-edit"></i></a>
                              <a href="<?php echo $url?>!/<?php echo $link[1]?>/deletar/<?php echo $row['midia_id']?>" class="btn btn-sm btn-outline-danger"><i class="fa fa-times"></i></a>
                          </td>
                        </tr>
                       
                       
                        <?php
                        }
                        ?>
    
                      </tbody>
                    </table>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    var $tabelas = jQuery.noConflict(); 
    $tabelas(document).ready(function() {

        $tabelas('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

