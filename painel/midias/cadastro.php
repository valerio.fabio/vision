<h2>Cadastro &bull; Midia</h2>

<hr>

<a class="btn btn-outline-warning" href="<?php echo $url?>!/<?php echo $link[1]?>/visualizar">Voltar</a>

<hr>

<form enctype="multipart/form-data" id="cadastro" method="post">
<table class="table">
  <tr>
    <th width="200">Link</th>
    <td valign="middle"><input class="form-control" name="midia_link" type="text" id="midia_link" maxlength="200" /></td>
    </tr>
  <tr>
    <th>Tipo</th>
    <td>
    <select name="midia_tipo" id="midia_tipo" class="form-control">
      <option value="fab fa-facebook-f">Facebook</option>
      <option value="fab fa-instagram">Instagram</option>
      <option value="fab fa-youtube">Youtube</option>
      <option value="fab fa-whatsapp">Whatsapp</option>
      <option value="fab fa-linkedin">Linkedin</option>
    </select>
    </td>
  </tr>

 

  <tr>
    <td>
    <input type="hidden" name="tabela" value="midias"> 
    <input type="hidden" name="url" value="<?php echo $url ?>">
    <input type="hidden" name="redirecionar" value="visualizar">
    <input type="hidden" name="tinyMCE" value="false"> 
    <input type="button" name="Enviar" id="salvar" value="Enviar" class="btn btn-outline-success" />
  </td>
  </tr>
</table>
</form>

<div id="result"></div>

<script src="<?php echo $url ?>php/db.class.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript" language="javascript">
  /* REGISTRO */
  cadastro('<?php echo $url; ?>', '');
</script>

