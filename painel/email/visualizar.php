<?php
$vis = new db();
$vis->query( "SELECT * FROM email ORDER BY mail_email ASC" );
$vis->execute();
?>

<h2 class="display-4 mb-3">Configuração de Email<small> (<?php echo $vis->rowCount()?>)</small></h2>

<hr>
<div class="card">
  <div class="card-body">

    <table class="table table-striped table-hover" id="dataTables-example">
      <thead>
        <tr>
          <th width="20">ID</th>
          <th>Email</th>
          <th width="220" align="right"></th>
        </tr>
      </thead>
      <tbody>
        <?php
        if( !empty($vis->row()) ){
          foreach( $vis->row() AS $row ){
            ?>

            <tr class="odd gradeX text-center">
              <td><?php echo $row['mail_id']?></td>
              <td class="text-left"><?php echo $row['mail_email']?></td>
              <td align="right" >
                <a target="_new" href="<?php echo $url?>/email/teste.php" class="btn btn-sm btn-outline-info btn-md"><i class="fas fa-envelope-open-text"></i></a>
                <a href="<?php echo $url?>!/<?php echo $link[1]?>/editar/<?php echo $row['mail_id']?>" class="btn btn-sm btn-outline-success btn-md"><i class="fas fa-edit"></i></a>
                <a href="<?php echo $url?>!/<?php echo $link[1]?>/deletar/<?php echo $row['mail_id']?>" class="btn btn-sm btn-outline-danger btn-md"><i class="fas fa-trash-alt"></i></a>
              </td>
            </tr>
            <?php
          }
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
