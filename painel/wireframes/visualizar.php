<link href="assets/list_dir/file-tree.min.css" rel="stylesheet">



<a class="btn btn-warning" href="<?php echo $url?>!/<?php echo $link[1]?>/visualizar">Voltar</a>
<a class="btn btn-info" href="<?php echo $url?>!/<?php echo $link[1]?>/upar">Upar</a>

<hr>

<h2>Wireframes Páginas</h2>

<div class="p-4 my-3 badge-danger">
	<h2 class="lead">Qualquer a&ccedil;&atilde;o realizada, n&atilde;o poder&aacute; voltar!!!</h2>
</div>

<hr>

<?php
if( !empty( $_GET['edicao'] ) ){
   echo '<div class="my-3 card card-body"><strong>Você acabou de editar</strong> '.$_GET['edicao'].'</div>';
}
?>


<?php

$dir = "configuracao/wireframes/";
$dirClone = "configuracao/wireframes-bkp/";

function LerPasta($Pasta){ 
	$LerDiretorio = opendir($Pasta) or die('Erro');
	while($Entry = @readdir($LerDiretorio)) { 

		if( !strstr($Entry, '#') ){
			if(is_dir($Pasta.'/'.$Entry)&& $Entry != '.' && $Entry != '..') { 

				$montar .= "{
								id: 'dir-".$Entry."',
								name: '".$Entry."',
								type: 'dir',
								children: [";
								$montar .=     LerPasta($Pasta.'/'.$Entry);
				$montar .= "	  		  ]
							},";
				
			} else { 
				if( $Entry != '..' && $Entry != '.' ){
					$montar .= "{
									id: 'file-".$Entry."',
									name: '".$Entry."',
									type: 'zip',
									url: '".$url."!/wireframes/editar&file=".$Pasta."/".$Entry."'
								},";
				}
			} 
		}

	} 
	return $montar;
	closedir($LerDiretorio);
} 

?>

	<div class="easy-tree card card-body">
		<?php //LerPasta($dir); ?>
		<div id="fixed-tree"></div>
    </div>


<script src="assets/list_dir/file-tree.min.js"></script> 
<script type="text/javascript">
    $(document).ready(function(){


		//Lista
        var data = [{
            id: 'dir-wf',
            name: 'WireFrames',
            type: 'dir',
			children:[
				<?php echo LerPasta($dir); ?>
			]
            // children: [
            //     {
            //         id: 'dir-2',
            //         name: 'Sub_dir',
            //         type: 'dir',
            //         children: [{
            //             id: 'file-1',
            //             name: 'file-tree-master.zip',
            //             type: 'zip',
            //             url: 'http://www.jqueryscript.net/other/Nice-File-Tree-View-Plugin-with-jQuery-Bootstrap-File-Tree.html'
            //         }]
            //     },
            //     {
            //         id: 'dir-2',
            //         name: 'Sub_dir',
            //         type: 'dir',
            //         children: [{
            //             id: 'file-1',
            //             name: 'file-tree-master.zip',
            //             type: 'zip',
            //             url: 'http://www.jqueryscript.net/other/Nice-File-Tree-View-Plugin-with-jQuery-Bootstrap-File-Tree.html'
            //         }]
            //     },
            //     {
            //         id: 'file-2',
            //         name: 'File tree',
            //         type: 'zip',
            //         url: 'http://www.jqueryscript.net/other/Nice-File-Tree-View-Plugin-with-jQuery-Bootstrap-File-Tree.html'
            //     }
            // ]
        }];

        $('#fixed-tree').fileTree({
            data: data
        });

        var sortableTree = $('#sortable-tree').fileTree({
            data: data,
            sortable: true
        });

        var selectableTree = $('#selectable-tree').fileTree({
            data: data,
            selectable: true
        });

        // selectableTree.bind('itemSelected', function(e, el){
        //     var output = 'Selected item:' + "\n";
        //     output += 'ID: ' + $(el).data('id') + "\n";
        //     output += 'Name: ' + $(el).data('name') + "\n";
        //     output += 'Type: ' + $(el).data('type');
        //     $('#selectable-tree-output').val(output);
        // });

        // $('#sortable-tree-toObject').bind('click', function(){
        //     $('#sortable-tree-output').val('Inspect the object in your browser console.');
        //     console.log(sortableTree.fileTree('toObject'));
        // });

        // $('#sortable-tree-toJson').bind('click', function(){
        //     var output = sortableTree.fileTree('toJson');
        //     $('#sortable-tree-output').val(output);
        //     console.log(output);
        // });

        <?php
        if( !empty( $_GET['edicao'] ) ){
            $listandoUrl = @explode('/', $_GET['edicao']);
        ?>
          $('#dir-wf').addClass('mjs-nestedSortable-expanded');
          $('#dir-wf').removeClass('mjs-nestedSortable-collapsed');
          
          $('#dir-<?php echo $listandoUrl[2] ?>').addClass('mjs-nestedSortable-expanded');
          $('#dir-<?php echo $listandoUrl[2] ?>').removeClass('mjs-nestedSortable-collapsed');
          
          $('#dir-<?php echo $listandoUrl[2] ?> #dir-1').addClass('mjs-nestedSortable-expanded');
          $('#dir-<?php echo $listandoUrl[2] ?> #dir-1').removeClass('mjs-nestedSortable-collapsed');
          
          $('#file-<?php echo $listandoUrl[4] ?>').addClass('text-danger');
          
        <?php
        }
        ?>

    });
</script>
