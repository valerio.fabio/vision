


<a class="btn btn-warning" href="<?php echo $url?>!/<?php echo $link[1]?>/visualizar">Voltar</a>
<a class="btn btn-info" href="<?php echo $url?>!/<?php echo $link[1]?>/upar">Upar</a>

<hr>

<h2>Wireframes Páginas</h2>

<div class="p-4 my-3 badge-danger">
	<h2 class="lead">Qualquer a&ccedil;&atilde;o realizada, n&atilde;o poder&aacute; voltar!!!</h2>
</div>

<hr>


<?php

$dir = "configuracao/wireframes/";
$dirClone = "configuracao/wireframes-bkp/";

function LerPasta($Pasta){ 
	$LerDiretorio = opendir($Pasta) or die('Erro');
	while($Entry = @readdir($LerDiretorio)) { 

		if( !strstr($Entry, '#') ){
			if(is_dir($Pasta.'/'.$Entry)&& $Entry != '.' && $Entry != '..') { 
				echo '<ul>';
				echo '<li><a href="#"><i class="fas fa-folder"></i> '.$Entry."</a>";
				LerPasta($Pasta.'/'.$Entry);
				echo '</li>';
				echo '</ul>';
			} else { 
				if( $Entry != '..' && $Entry != '.' ){
					echo '<ul>';
					echo '<li>';
					echo '<!--<a class="del btn btn-danger btn-sm" href="deletar"><i class="far fa-trash-alt"></i></a> -->
					      <a class="del btn btn-success btn-sm"  href="'.$url.'restaurar&file='.$Pasta.'/'.$Entry.'&fileBKP='.str_replace('wireframes', 'wireframes-bkp', $Pasta).'/'.$Entry.'"><i class="fas fa-sync"></i></a> 
					      <a class="btn btn-sm btn-primary" href="'.$url.'editar&file='.$Pasta.'/'.$Entry.'">'.$Entry.'</a>';
					echo '</li>';
					echo '</ul>';
				}
			} 
		}

	} 
	closedir($LerDiretorio);
} 

?>

<div class="container-fluid">
	<div class="easy-tree">
        <?php LerPasta($dir); ?>
    </div>
</div>

