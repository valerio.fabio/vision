        <a class="navbar-brand" href="<?php echo $url; ?>">
          DASHBOARD
        </a>

        <ul class="nav  flex-column nav-pills nav-stacked">

          <li class="nav-item">
           <a class="nav-link <?php if( empty($link[1]) ) echo 'active'?>" href="<?php echo $url;?>">
            <i class="fa fa-home"></i> Home
          </a>
        </li>

        <li  data-toggle="collapse" data-target="#products" class="collapsed active nav-item">
          <a href="#" class="nav-link" onclick="javascript:return false">
            <i class="fa fa-cogs"></i>
            <span>Configura&ccedil;&otilde;es</span>
            <span style="float: right;"><i class="fa fa-plus"></i></span>
          </a>
        </li>

        <ul class="collapse" id="products">

          <?php if( $_COOKIE['user_id'] == '1' ){?>
            <li class="nav-item">
             <a  class="nav-link <?php if( $link[1] == 'menu' ) echo 'active'?>" href="<?php echo $url?>!/menu/visualizar">
              <i class="fa fa-angle-right"></i> Menu
            </a>
          </li>
        <?php }?>

        <li class="nav-item">
         <a  class="nav-link <?php if( $link[1] == 'banner' ) echo 'active'?>" href="<?php echo $url?>!/banner/visualizar">
          <i class="fa fa-angle-right"></i>  Banner
        </a>
      </li>

      <li class="nav-item">
       <a  class="nav-link <?php if( $link[1] == 'pagina_home' ) echo 'active'?>" href="<?php echo $url?>!/pagina_home/visualizar">
        <i class="fa fa-home"></i> Home do site
      </a>
    </li>

    <?php if(  $jsonConf->c_categoria == '1' ){ ?>
      <li class="nav-item">
       <a  class="nav-link <?php if( $link[1] == 'paginas_categoria' ) echo 'active'?>" href="<?php echo $url?>!/paginas_categoria/visualizar">
        <i class="fa fa-angle-right"></i> Categoria P&aacute;ginas
      </a>
    </li>
  <?php }?>

  <?php if(  $jsonConf->c_subcategoria == '1' ){ ?>
    <li class="nav-item">
     <a  class="nav-link <?php if( $link[1] == 'paginas_subcategoria' ) echo 'active'?>" href="<?php echo $url?>!/paginas_subcategoria/visualizar">
      <i class="fa fa-angle-right"></i> Subcategoria P&aacute;ginas
    </a>
  </li>
<?php }?>

<li class="nav-item">
 <a  class="nav-link <?php if( $link[1] == 'images-json' ) echo 'active'?>" href="<?php echo $url?>!/images-json/fotos">
  <i class="fa fa-angle-right"></i>  Biblioteca de Imagens
</a>
</li>

</ul>

<li class="nav-item">
 <a  class="nav-link <?php if( $link[1] == 'paginas' ) echo 'active'?>" href="<?php echo $url?>!/paginas/visualizar">
  <i class="fas fa-paper-plane"></i> P&aacute;ginas
</a>
</li>

<li class="nav-item">
 <a  class="nav-link <?php if( $link[1] == 'textos' ) echo 'active'?>" href="<?php echo $url?>!/textos/visualizar">
  <i class="fas fa-font"></i> Textos
</a>
</li>

<li class="nav-item">
 <a  class="nav-link <?php if( $link[1] == 'contatos' ) echo 'active'?>" href="<?php echo $url?>!/contatos/visualizar">
  <i class="fa fa-envelope"></i> E-mails
</a>
</li>

<?php if(  $jsonConf->c_news == '1' ){ ?>
  <li class="nav-item">
   <a  class="nav-link <?php if( $link[1] == 'newsletter' ) echo 'active'?>" href="<?php echo $url?>!/newsletter/visualizar">
    <i class="fas fa-newspaper"></i> Newsletter
  </a>
</li>
<?php }?>

<li class="text-white p-2 lead">
  <hr>
  Desenvolvedor
</li>

<?php if(  $jsonConf->c_land == '1' ){ ?>
  <li class="nav-item">
   <a  class="nav-link <?php if( $link[1] == 'landpage' ) echo 'active'?>" href="<?php echo $url?>!/landpage/visualizar">
    <i class="fa fa-sitemap"></i> LandPage
  </a>
</li>
<?php }?>

<?php if(  $jsonConf->c_seo == '1' ){ ?>
  <li class="nav-item">
   <a  class="nav-link <?php if( $link[1] == 'seo' ) echo 'active'?>" href="<?php echo $url?>!/seo/visualizar">
    <i class="fas fa-puzzle-piece"></i> SEO
  </a>
</li>
<?php }?>

<?php if(  $jsonConf->c_css == '1' ){ ?>
  <li class="nav-item">
   <a  class="nav-link <?php if( $link[1] == 'css' ) echo 'active'?>" href="<?php echo $url?>!/css/visualizar">
    <i class="fab fa-css3"></i> Editor CSS
  </a>
</li>
<?php }?>

<?php if(  $jsonConf->c_dev == '1' ){ ?>
  <li class="nav-item">
   <a  class="nav-link <?php if( $link[1] == 'wireframes' ) echo 'active'?>" href="<?php echo $url?>!/wireframes/visualizar">
    <i class="fab fa-dev"></i> &Aacute;rea do Desenvolvedor
  </a>
</li>
<?php }?>

</ul>