


<nav class="navbar navbar-fixed-top navbar-dark d-flex flex-row-reverse fixed-top">
    <div class="mx-3">
        <div class="row d-flex align-items-center">

            <div class="col-md-6">
             <?php echo $jsonConf->c_nome; ?> 
         </div>
         <div class="col-md-6">
             <div class="dropdown">
                <button type="button" id="DropConf" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-cog"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-top dropdown-menu-tip-nw dropdown-menu-right" aria-labelledby="DropConf">
                <a class="dropdown-item" href="<?php echo $url?>!/usuarios/visualizar"><i class="fa fa-user"></i> Usu&aacute;rios</a>
                <?php if( $_COOKIE['user_id'] == '1' ){ ?>
                    <a class="dropdown-item" href="<?php echo $url?>!/configuracao/visualizar" data-toggle="modal" data-target=".bd-example-modal-xl"><i class="fa fa-cogs"></i> Geral</a>
                    <div class="dropdown-divider"></div>
                    <p class="dropdown-item disabled m-0">Logos</p>
                    <a class="dropdown-item" href="<?php echo $url?>!/logo/principal"><i class="fa fa-image"></i> Nav</a>
                    <a class="dropdown-item" href="<?php echo $url?>!/logo/footer"><i class="fa fa-image"></i> Footer</a>
                    <a class="dropdown-item" href="<?php echo $url?>!/logo/favicon"><i class="fa fa-image"></i> FavIcon</a>
                    <a class="dropdown-item" href="<?php echo $url?>!/logo/landing"><i class="fa fa-image"></i> LandingPages</a>
                    <div class="dropdown-divider"></div>
                    <p class="dropdown-item disabled m-0">Wireframes</p>
                    <a class="dropdown-item" href="<?php echo $url?>!/wireframes/visualizar"><i class="fa fa-flask fa-fw"></i> P&aacute;ginas</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo $url?>!/email/visualizar"><i class="fa fa-envelope"></i> Email</a>
                    <div class="dropdown-divider"></div>
                <?php } ?>
                <a class="dropdown-item" href="<?php echo $url?>!/midias/visualizar"><i class="fa fa-share-alt fa-fw"></i> M&iacute;dia</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?php echo $url?>sair"><i class="fa fa-power-off"></i> Logout</a>
            </div>
        </div>
    </div>
</div>
</nav>

<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" style="max-width: 90vw;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Configurações</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div id="modal-configuracao"></div>
      </div>
    </div>
  </div>
</div>

<script>
     $.get( "configuracao/visualizar.php", function( data ) {
        jQuery("#modal-configuracao").html("Carregando ....");
        jQuery("#modal-configuracao").html(data);
    });
</script>